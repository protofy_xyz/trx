class User{
    name: string;
    email: string;
    token: string;
    type: string;
    status: string;

    constructor(name: string, email: string, token: string, type: string, status?: string) {
        this.name = name;
        this.email = email;
        this.token = token;
        this.type = type;
        this.status = status ? status : "";
    }

    load(data: any): User {
        return new User(data.name, data.email, data.token, data.type, data.status);
    }

    isEmpty(): boolean {
        return this.email == undefined;
    }
}

export default User
