# MonoRepo for Ionic typescript system

apps/ contains the visual applications for the project. It includes an example 'client' application
environ/ contains the docker-compose file, to start a development environment
services/ contains the backend services, It includes an API examples

## How to build

There is a file called build.sh, in environ folder; that builds the system.

```bash
$ ./build.sh
```

## Run in development mode

There is a file called dev.sh, in environ folder; that builds the system.

```bash
$ ./dev.sh
```
## Run in production mode

```bash
$ ./prod.sh
```

Both **backends** and **frontends** are automaticly rebuilded when there is a file change.

