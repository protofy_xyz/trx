"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var firmata_1 = require("firmata");
var motor_1 = require("./models/motor");
var motor;
var isBoardReady = false;
var createBoard = function () {
    firmata_1.Board.requestPort(function (error, port) {
        if (error) {
            console.log(error);
            return;
        }
        console.log('Using port: ', port.path);
        var board = new firmata_1.Board(port.path, function () {
            console.log("BOARD READY");
            isBoardReady = true;
            motor = new motor_1.Motor(board);
        });
    });
};
createBoard();
var express = require('express');
var app = express();
var port = 4000;
var cors = require('cors');
app.use(cors({ origin: "*" }));
var WebSocketServer = require('websocket').server;
var http = require('http');
var server = http.createServer();
server.listen(9898);
var wsServer = new WebSocketServer({
    httpServer: server
});
var broadCast = function (msg) {
    sockets.forEach(function (socket) {
        socket.sendUTF(msg);
    });
};
var sockets = [];
wsServer.on('request', function (request) {
    var connection = request.accept(null, request.origin);
    sockets.push(connection);
    connection.on('close', function (reasonCode, description) {
        console.log('Client has disconnected.');
        sockets = sockets.filter(function (socket) { return socket != connection; });
    });
});
setInterval(function () {
    broadCast('ping');
}, 30000);
app.get('/board/status/', function (req, res) {
    res.status(200);
    res.json({ "result": isBoardReady });
});
app.get('/output/enable/:status/', function (req, res) {
    var status = req.params.status;
    if (status == "on") {
        motor.enableMotor(1);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else if (status == "off") {
        motor.enableMotor(0);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else {
        res.status(500);
        res.json({ "message": "Status incorrect" });
    }
});
app.get('/output/direction/:status/', function (req, res) {
    var status = req.params.status;
    if (status == "on") {
        motor.setDirection(1);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else if (status == "off") {
        motor.setDirection(0);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else {
        res.status(500);
        res.json({ "message": "Status incorrect" });
    }
});
app.get('/output/torque/:value/', function (req, res) {
    var value = req.params.value;
    if (value == undefined || value < 0 || value > 255) {
        res.status(500);
        res.json({ "message": "Incorrect value" });
    }
    else {
        console.log("Torque: ", 255 - parseInt(value));
        //motor.setTorque(255 - parseInt(value));
        res.status(200);
        res.json({ "result": "ok" });
    }
});
app.get('/output/speed/:value/', function (req, res) {
    var value = req.params.value;
    if (value == undefined || value < 0 || value > 255) {
        res.status(500);
        res.json({ "message": "Incorrect value" });
    }
    else {
        console.log("Speed: ", value);
        motor.setSpeed(parseInt(value));
        res.status(200);
        res.json({ "result": "ok" });
    }
});
app.get('/brake/', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        console.log("--- BRAKE ---");
        motor.brake(1);
        res.status(200);
        res.json({ "result": "ok" });
        return [2 /*return*/];
    });
}); });
app.get('/modes/constantSpeed/:speed/:direction/:exerciseDuration/', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var speed, direction, exerciseDuration;
    return __generator(this, function (_a) {
        speed = parseInt(req.params.speed, 10);
        direction = parseInt(req.params.direction, 10);
        exerciseDuration = parseInt(req.params.exerciseDuration, 10);
        motor.constantSpeed(speed, direction, exerciseDuration);
        res.status(200);
        res.json({ "result": "ok" });
        return [2 /*return*/];
    });
}); });
app.get('/modes/randomTurns/:minSpeed/:maxSpeed/:minTimeBetweenExercises/:maxTimeBetweenExercises/:duration/', function (req, res) {
    var minSpeed = parseInt(req.params.minSpeed, 10);
    var maxSpeed = parseInt(req.params.maxSpeed, 10);
    var minTimeBetweenExercises = parseInt(req.params.minTimeBetweenExercises, 10);
    var maxTimeBetweenExercises = parseInt(req.params.maxTimeBetweenExercises, 10);
    var duration = parseInt(req.params.duration, 10);
    motor.randomTurns(minSpeed, maxSpeed, minTimeBetweenExercises, maxTimeBetweenExercises, duration);
    res.status(200);
    res.json({ "result": "ok" });
});
app.get('/modes/randomTicks/:minSpeed/:maxSpeed/:minTimeBetweenExercises/:maxTimeBetweenExercises/:minExerciseDurationTime/:maxExerciseDurationTime/:duration', function (req, res) {
    var minSpeed = parseInt(req.params.minSpeed, 10);
    var maxSpeed = parseInt(req.params.maxSpeed, 10);
    var minTimeBetweenExercises = parseInt(req.params.minTimeBetweenExercises, 10);
    var maxTimeBetweenExercises = parseInt(req.params.maxTimeBetweenExercises, 10);
    var minExerciseDurationTime = parseFloat(req.params.minExerciseDurationTime);
    var maxExerciseDurationTime = parseFloat(req.params.maxExerciseDurationTime);
    var duration = parseInt(req.params.duration, 10);
    motor.randomTicks(minSpeed, maxSpeed, minTimeBetweenExercises, maxTimeBetweenExercises, minExerciseDurationTime, maxExerciseDurationTime, duration);
    res.status(200);
    res.json({ "result": "ok" });
});
app.listen(port, function () { return console.log("Example app listening at http://localhost:" + port); });
