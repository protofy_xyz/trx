import { Board } from 'firmata'
import { Motor } from './models/motor'

var motor;
var isBoardReady = false;

const createBoard = () => {
    Board.requestPort((error, port) => {
        if (error) {
            console.log(error);
            return;
        }

        console.log('Using port: ', port.path);

        var board = new Board(port.path, function () {
            console.log("BOARD READY");
            isBoardReady = true;
            motor = new Motor(board);

        });
    });
}

createBoard()

const express = require('express');
const app = express();
const port = 4000;
const cors = require('cors');

app.use(cors({ origin: "*" }));

const WebSocketServer = require('websocket').server;
const http = require('http');
const server = http.createServer();
server.listen(9898);

const wsServer = new WebSocketServer({
    httpServer: server
});

const broadCast = (msg) => {
    sockets.forEach(socket => {
        socket.sendUTF(msg);
    });
}

var sockets = [];
wsServer.on('request', function (request) {
    const connection = request.accept(null, request.origin);
    sockets.push(connection);
    connection.on('close', function (reasonCode, description) {
        console.log('Client has disconnected.');
        sockets = sockets.filter(socket => socket != connection)
    });
});


setInterval(() => {
    broadCast('ping');
}, 30000);

app.get('/board/status/', (req, res) => {
    res.status(200);
    res.json({ "result": isBoardReady });
})

app.get('/output/enable/:status/', (req, res) => {
    var status = req.params.status;
    if (status == "on") {
        motor.enableMotor(1);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else if (status == "off") {
        motor.enableMotor(0);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else {
        res.status(500);
        res.json({ "message": "Status incorrect" })
    }
})

app.get('/output/direction/:status/', (req, res) => {
    var status = req.params.status;
    if (status == "on") {
        motor.setDirection(1);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else if (status == "off") {
        motor.setDirection(0);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else {
        res.status(500);
        res.json({ "message": "Status incorrect" })
    }
})

app.get('/output/torque/:value/', (req, res) => {
    var value = req.params.value;
    if (value == undefined || value < 0 || value > 255) {
        res.status(500);
        res.json({ "message": "Incorrect value" });
    }
    else {
        console.log("Torque: ", 255 - parseInt(value));
        //motor.setTorque(255 - parseInt(value));
        res.status(200);
        res.json({ "result": "ok" })
    }
})

app.get('/output/speed/:value/', (req, res) => {
    var value = req.params.value;
    if (value == undefined || value < 0 || value > 255) {
        res.status(500);
        res.json({ "message": "Incorrect value" });
    }
    else {
        console.log("Speed: ", value);
        motor.setSpeed(parseInt(value));
        res.status(200);
        res.json({ "result": "ok" })
    }
})

app.get('/brake/', async (req, res) => {
    console.log("--- BRAKE ---");
    motor.brake(1);
    res.status(200);
    res.json({ "result": "ok" });
})

app.get('/modes/constantSpeed/:speed/:direction/:exerciseDuration/', async (req, res) => { //Mode velocitat constant
    var speed = parseInt(req.params.speed, 10);
    var direction = parseInt(req.params.direction, 10);
    var exerciseDuration = parseInt(req.params.exerciseDuration, 10);

    motor.constantSpeed(speed, direction, exerciseDuration);

    res.status(200);
    res.json({ "result": "ok" });
})

app.get('/modes/randomTurns/:minSpeed/:maxSpeed/:minTimeBetweenExercises/:maxTimeBetweenExercises/:duration/', (req, res) => {
    var minSpeed = parseInt(req.params.minSpeed, 10);
    var maxSpeed = parseInt(req.params.maxSpeed, 10);
    var minTimeBetweenExercises = parseInt(req.params.minTimeBetweenExercises, 10);
    var maxTimeBetweenExercises = parseInt(req.params.maxTimeBetweenExercises, 10);
    var duration = parseInt(req.params.duration, 10);

    motor.randomTurns(minSpeed, maxSpeed, minTimeBetweenExercises, maxTimeBetweenExercises, duration);
    res.status(200);
    res.json({ "result": "ok" })
})

app.get('/modes/randomTicks/:minSpeed/:maxSpeed/:minTimeBetweenExercises/:maxTimeBetweenExercises/:minExerciseDurationTime/:maxExerciseDurationTime/:duration', (req, res) => {
    var minSpeed = parseInt(req.params.minSpeed, 10);
    var maxSpeed = parseInt(req.params.maxSpeed, 10);
    var minTimeBetweenExercises = parseInt(req.params.minTimeBetweenExercises, 10);
    var maxTimeBetweenExercises = parseInt(req.params.maxTimeBetweenExercises, 10);
    var minExerciseDurationTime = parseFloat(req.params.minExerciseDurationTime);
    var maxExerciseDurationTime = parseFloat(req.params.maxExerciseDurationTime);
    var duration = parseInt(req.params.duration, 10);

    motor.randomTicks(minSpeed, maxSpeed, minTimeBetweenExercises, maxTimeBetweenExercises, minExerciseDurationTime, maxExerciseDurationTime, duration);
    res.status(200);
    res.json({ "result": "ok" })
})


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
