"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.Motor = void 0;
var MOTORTRX_DATA = 0x50;
var MOTORTRX_ATTACH = 0x00;
var MOTORTRX_ENABLE = 0x01;
var MOTORTRX_DIRECTION = 0x02;
var MOTORTRX_SPEED = 0x03;
var MOTORTRX_BRAKE = 0x04;
var MOTORTRX_BRAKE1000 = 0x05;
var interval;
var Motor = /** @class */ (function () {
    function Motor(board) {
        this.board = board;
        this.actualDirection = 0;
        this.dynamicTorqueMax = 0;
        this.dynamicTorqueMin = 120;
        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_ATTACH]);
        this.mode = "none";
    }
    ;
    Motor.prototype.timeout = function (ms) {
        return new Promise(function (resolve) { return setTimeout(resolve, ms); });
    };
    Motor.prototype.enableMotor = function (status) {
        console.log("Enable: ", status);
        this.motorIsEnabled = status;
        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_ENABLE, status]);
    };
    ;
    Motor.prototype.setDirection = function (status) {
        console.log("Direction: ", status);
        this.actualDirection = status;
        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_DIRECTION, status]);
    };
    ;
    Motor.prototype.setSpeed = function (value) {
        console.log("Speed: ", value);
        this.actualSpeed = value;
        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_SPEED, value]);
    };
    ;
    Motor.prototype.getRandomArbitrary = function (min, max, integer) {
        if (integer)
            return Math.floor((Math.random() * (max - min + 1)) + min);
        else
            return ((Math.random() * (max - min + 1)) + min);
    };
    Motor.prototype.brake = function (value) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("Brake: ", value);
                        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_BRAKE1000, value]);
                        return [4 /*yield*/, this.timeout(value * 1000)];
                    case 1:
                        _a.sent();
                        this.setSpeed(0);
                        this.enableMotor(0);
                        return [2 /*return*/];
                }
            });
        });
    };
    Motor.prototype.constantSpeed = function (speed, direction, totalDuration) {
        var _this = this;
        this.mode = "constantSpeed";
        this.setSpeed(speed);
        this.setDirection(direction);
        this.enableMotor(1);
        setTimeout(function () {
            _this.enableMotor(0);
            _this.setSpeed(0);
            _this.mode = "none";
        }, totalDuration * 1000);
    };
    ;
    Motor.prototype.doATurn = function (minSpeed, maxSpeed, randomDirection) {
        return __awaiter(this, void 0, void 0, function () {
            var randomSpeed;
            return __generator(this, function (_a) {
                this.mode = "randomTurns";
                randomSpeed = this.getRandomArbitrary(minSpeed, maxSpeed, true);
                this.setSpeed(randomSpeed);
                this.setDirection(randomDirection);
                return [2 /*return*/];
            });
        });
    };
    Motor.prototype.randomTurns = function (minSpeed, maxSpeed, minTimeBetweenExercises, maxTimeBetweenExercises, totalDuration) {
        var _this = this;
        this.mode = "randomTurns";
        var movingTime = this.getRandomArbitrary(minTimeBetweenExercises, maxTimeBetweenExercises, true);
        var randomDirection = Math.floor(Math.random() * (2));
        this.setSpeed(0);
        this.enableMotor(1);
        interval = setInterval(function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                randomDirection = (this.actualDirection + 1) % 2;
                this.doATurn(minSpeed, maxSpeed, randomDirection);
                return [2 /*return*/];
            });
        }); }, movingTime * 1000);
        setTimeout(function () {
            clearInterval(interval);
            _this.mode = "none";
            _this.enableMotor(0);
            _this.setSpeed(0);
        }, totalDuration * 1000);
    };
    Motor.prototype.doATick = function (minSpeed, maxSpeed, exerciseDurationTime) {
        return __awaiter(this, void 0, void 0, function () {
            var randomSpeed;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.mode = "randomTicks";
                        console.log("----ITERATION-------");
                        randomSpeed = this.getRandomArbitrary(minSpeed, maxSpeed, true);
                        this.setSpeed(randomSpeed);
                        if (!(this.mode == "randomTicks")) return [3 /*break*/, 2];
                        console.log(new Date().toLocaleString(), "----DIRECTION 1-------");
                        this.setDirection(this.getRandomArbitrary(0, 1, true));
                        this.enableMotor(1);
                        return [4 /*yield*/, this.timeout(exerciseDurationTime * 1000 / 2 * 0.95)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!(this.mode == "randomTicks")) return [3 /*break*/, 4];
                        console.log(new Date().toLocaleString(), "----DIRECTION 2-------");
                        this.setDirection((this.actualDirection + 1) % 2);
                        this.enableMotor(1);
                        return [4 /*yield*/, this.timeout(exerciseDurationTime * 1000 / 2 / 0.95)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        if (this.mode == "randomTicks") {
                            this.brake(1);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    Motor.prototype.randomTicks = function (minSpeed, maxSpeed, minTimeBetweenExercises, maxTimeBetweenExercises, minExerciseDurationTime, maxExerciseDurationTime, totalDuration) {
        var _this = this;
        this.mode = "randomTicks";
        var timeBetweenExercises = this.getRandomArbitrary(minTimeBetweenExercises, maxTimeBetweenExercises, true);
        var exerciseDurationTime = this.getRandomArbitrary(minExerciseDurationTime * 10, maxExerciseDurationTime * 10, true) / 10;
        this.setSpeed(0);
        this.enableMotor(1);
        this.doATick(minSpeed, maxSpeed, exerciseDurationTime);
        interval = setInterval(function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.doATick(minSpeed, maxSpeed, exerciseDurationTime)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); }, exerciseDurationTime * 1000 + timeBetweenExercises * 1000);
        setTimeout(function () {
            clearInterval(interval);
            _this.mode = "none";
            _this.enableMotor(0);
            _this.setSpeed(0);
        }, totalDuration * 1000);
    };
    return Motor;
}());
exports.Motor = Motor;
;
