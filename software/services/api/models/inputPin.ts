import { Board } from 'firmata';

export class InputPin{
    pin: number;
    lastValue: number;
    value: number;
    lastMillis: number;
    edge:  string;
    holdMillis: number;
    board: Board;

    constructor(pin, board, holdMillis) {
        this.pin = pin;
        this.lastValue=2;
        this.value;
        this.lastMillis = 0;
        this.edge;
        this.holdMillis = holdMillis;
        this.board = board;
        this.board.pinMode(this.pin, this.board.MODES.INPUT);
    }
    
    valueChanged(newValue) {
        if ((new Date().getTime() - this.lastMillis) > this.holdMillis) {
            this.lastValue = this.value;
            this.value = newValue;
            this.lastMillis = new Date().getTime();
            
            if (this.lastValue == 0 && this.value == 1) {
                this.edge = 'R';
                return true;
            }else if (this.lastValue == 1 && this.value == 0) {
                this.edge = 'F';
                return true;
            } else {
                this.edge = 'X';
                return false;
            }
        }
        return false;
    }
    isRising() {
        return this.edge == 'R';
    }
    isFalling() {
        return this.edge == 'F';
    }
}