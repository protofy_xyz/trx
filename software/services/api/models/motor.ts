import { Board } from 'firmata';
import { InputPin } from './inputPin'

const MOTORTRX_DATA = 0x50
const MOTORTRX_ATTACH = 0x00
const MOTORTRX_ENABLE = 0x01
const MOTORTRX_DIRECTION = 0x02
const MOTORTRX_SPEED = 0x03
const MOTORTRX_BRAKE = 0x04
const MOTORTRX_BRAKE1000 = 0x05

var interval: any;


export class Motor {
    board: Board;


    mode: String;

    torquePinSensor: InputPin;
    rotationDirectionPinSensor: InputPin;

    actualDirection: number;
    actualTorque: number;
    actualSpeed: number;
    motorIsEnabled: number;

    dynamicTorqueMax: number;
    dynamicTorqueMin: number;

    constructor(board: Board) {
        this.board = board;



        this.actualDirection = 0;

        this.dynamicTorqueMax = 0;
        this.dynamicTorqueMin = 120;

        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_ATTACH]);

        this.mode = "none";
    };

    timeout(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }


    enableMotor(status: number) {
        console.log("Enable: ", status);
        this.motorIsEnabled = status;
        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_ENABLE, status]);
    };


    setDirection(status: number) {
        console.log("Direction: ", status);
        this.actualDirection = status;
        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_DIRECTION, status]);
    };


    setSpeed(value: number) {
        console.log("Speed: ", value);
        this.actualSpeed = value;
        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_SPEED, value]);
    };

    getRandomArbitrary(min: number, max: number, integer: boolean) {
        if (integer) return Math.floor((Math.random() * (max - min + 1)) + min);
        else return ((Math.random() * (max - min + 1)) + min)
    }

    async brake(value: number) {
        console.log("Brake: ", value);
        this.board.sysexCommand([MOTORTRX_DATA, MOTORTRX_BRAKE1000, value]);
        await this.timeout(value * 1000);
        this.setSpeed(0);
        this.enableMotor(0);
    }


    constantSpeed(speed: number, direction: number, totalDuration: number) { //constant speed
        this.mode = "constantSpeed";
        this.setSpeed(speed);
        this.setDirection(direction)
        this.enableMotor(1);

        setTimeout(() => {
            this.enableMotor(0);
            this.setSpeed(0);
            this.mode = "none";
        }, totalDuration * 1000);

    };

    async doATurn(minSpeed: number, maxSpeed: number, randomDirection: number) {
        this.mode = "randomTurns";
        var randomSpeed = this.getRandomArbitrary(minSpeed, maxSpeed, true);
        this.setSpeed(randomSpeed);
        this.setDirection(randomDirection);
    }

    randomTurns(minSpeed: number, maxSpeed: number, minTimeBetweenExercises: number, maxTimeBetweenExercises: number, totalDuration: number) { //ticks random esquerra dreta
        this.mode = "randomTurns";
        var movingTime = this.getRandomArbitrary(minTimeBetweenExercises, maxTimeBetweenExercises, true);
        var randomDirection = Math.floor(Math.random() * (2));
        this.setSpeed(0);
        this.enableMotor(1);

        interval = setInterval(async () => {
            randomDirection = (this.actualDirection + 1) % 2;
            this.doATurn(minSpeed, maxSpeed, randomDirection);
        }, movingTime * 1000);

        setTimeout(() => {
            clearInterval(interval);
            this.mode = "none";
            this.enableMotor(0);
            this.setSpeed(0);
        }, totalDuration * 1000);
    }

    async doATick(minSpeed: number, maxSpeed: number, exerciseDurationTime: number) {
        this.mode = "randomTicks";

        console.log("----ITERATION-------");
        var randomSpeed = this.getRandomArbitrary(minSpeed, maxSpeed, true);
        this.setSpeed(randomSpeed);
        if (this.mode == "randomTicks") {
            console.log(new Date().toLocaleString(), "----DIRECTION 1-------");
            this.setDirection(this.getRandomArbitrary(0, 1, true));
            this.enableMotor(1);
            await this.timeout(exerciseDurationTime * 1000 / 2 * 0.95);
        }
        if (this.mode == "randomTicks") {
            console.log(new Date().toLocaleString(), "----DIRECTION 2-------");
            this.setDirection((this.actualDirection + 1) % 2);
            this.enableMotor(1);
            await this.timeout(exerciseDurationTime * 1000 / 2 / 0.95);
        }
        if (this.mode == "randomTicks") {
            this.brake(1);
        }
    }

    randomTicks(minSpeed: number, maxSpeed: number, minTimeBetweenExercises: number, maxTimeBetweenExercises: number, minExerciseDurationTime: number, maxExerciseDurationTime: number, totalDuration: number) {//ticks random esquerra dreta cada x temps

        this.mode = "randomTicks";

        var timeBetweenExercises = this.getRandomArbitrary(minTimeBetweenExercises, maxTimeBetweenExercises, true);
        var exerciseDurationTime = this.getRandomArbitrary(minExerciseDurationTime * 10, maxExerciseDurationTime * 10, true) / 10;

        this.setSpeed(0);
        this.enableMotor(1);

        this.doATick(minSpeed, maxSpeed, exerciseDurationTime);
        interval = setInterval(async () => {
            await this.doATick(minSpeed, maxSpeed, exerciseDurationTime);
        }, exerciseDurationTime * 1000 + timeBetweenExercises * 1000);

        setTimeout(() => {
            clearInterval(interval);
            this.mode = "none";
            this.enableMotor(0);
            this.setSpeed(0);
        }, totalDuration * 1000);
    }

};
