"use strict";
exports.__esModule = true;
exports.MotorOperation = void 0;
var MotorOperation = /** @class */ (function () {
    function MotorOperation(stepsPerRevolution, operationSpeed) {
        var _this = this;
        // this.initialOperationSteps = initialOperationSteps;
        // this.endOperationSteps = endOperationSteps;
        this.operationSpeed = operationSpeed;
        this.operation = new Promise(function (resolve, reject) {
            _this.operationResolve = resolve;
            _this.operationReject = reject;
        });
    }
    MotorOperation.prototype.resolveOperation = function () {
        this.operationResolve();
    };
    MotorOperation.prototype.rejectOperation = function (text) {
        this.operationReject(text);
    };
    return MotorOperation;
}());
exports.MotorOperation = MotorOperation;
