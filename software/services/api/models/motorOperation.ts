export class MotorOperation {
    stepsPerRevolution: number;
    operation: Promise<void>;

    operationResolve: any;
    operationReject: any;


    operationSpeed: number;


    constructor(stepsPerRevolution: number, operationSpeed: number) {
        this.operationSpeed = operationSpeed;

        this.operation = new Promise((resolve, reject)=>{
            this.operationResolve = resolve;
            this.operationReject = reject;
            
        });
    }

    resolveOperation(){
        this.operationResolve();
    }

    rejectOperation(text){
        this.operationReject(text);
    }
}