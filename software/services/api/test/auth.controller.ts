import 'mocha';
import { expect } from 'chai';
import request from 'supertest';
import Server from '../server';

describe('Auth', () => {
  it('should prevent login without credentials', () =>
  request(Server)
    .post('/api/v1/auth/login')
    .expect(415))

  it('should prevent login with incorrect password', () =>
    request(Server)
      .post('/api/v1/auth/login')
      .send({ email: 'test@test.com', password: 'test' })
      .expect(401))

  it('should return a signed User if provided with valid credentials', () =>
    request(Server)
      .post('/api/v1/auth/login')
      .send({ email: 'test@test.com', password: 'validpass' })
      .expect(200))
});
