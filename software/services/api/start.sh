#!/bin/sh
if [ -z "$DEV_MODE" ]
then
    echo "Production mode enabled"
    pm2 --no-daemon start dist/server/index.js
else
    echo "Development mode enabled"
    yarn install
    npm run dev
fi
