import { Application } from 'express';
import authRouter from './api/controllers/auth/authRouter';

export default function routes(app: Application): void {
  app.use('/api/v1/auth', authRouter);
}
