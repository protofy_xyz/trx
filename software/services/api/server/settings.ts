const env = process.env;
export default  {
    secret : env.JWT_KEY ? env.JWT_KEY : 'SECRET_CHANGE_ME_LOGIN',
    publicHost : env.PUBLIC_URL ? env.PUBLIC_URL : 'http://localhost',
    mongoAddr : env.MONGO_HOST ? env.MONGO_HOST : 'db',
    mongoUser: env.MONGO_USER ? env.MONGO_USER : '',
    mongoPassword: env.MONGO_PASSWORD ? env.MONGO_PASSWORD : '',
    mongoDbName: env.MONGO_DB_NAME ? env.MONGO_DB_NAME : 'api',
    version: env.VERSION ? env.VERSION : "1.0",
}