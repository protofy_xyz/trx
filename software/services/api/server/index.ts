import { init } from 'raspi';
import { DigitalInput, DigitalOutput } from 'raspi-gpio';
import { PWM } from 'raspi-pwm';

var delayInMilliseconds = 1000; //1 second

var enable;
var direction;
var maxTorque;
var maxSpeed;

const outputPin1 = "GPIO17"
const outputPin2 = "GPIO27"
const outputPin3 = "GPIO12";
const outputPin4 = "GPIO13";

const inputPin1 = "GPIO22";
// const inputPin2;
const inputPin3 = "GPIO23";

init(() => {
  enable = new DigitalOutput(outputPin1);
  direction = new DigitalOutput(outputPin2);
  maxTorque = new PWM(outputPin3);
  maxSpeed = new PWM(outputPin4);

  const rotationDirection = new DigitalInput(inputPin1);
//   const analogicInput = new PWM(inputPin2);
  const interruptions = new DigitalInput(inputPin3);

  rotationDirection.on("change", function(value) {
      broadCast("RotDirection changed to: " + value)
  })

//   analogicInput.on("change", function(value) {
//     broadCast("AnalogicInputanalogicInput changed to: " + value)
//   })

  interruptions.on("change", function(value) {
    broadCast("Interruptions changed to: " + value)
  })

});


const express = require('express')
const app = express()
const port = 3000 


const WebSocketServer = require('websocket').server;
const http = require('http');
const server = http.createServer();
server.listen(9898);

const wsServer = new WebSocketServer({
	httpServer: server
});

const broadCast = (msg) => {
	sockets.forEach(socket => {
		socket.sendUTF(msg);
	});
}

var sockets = [];
wsServer.on('request', function (request) {
	const connection = request.accept(null, request.origin);
	sockets.push(connection);
	connection.on('close', function (reasonCode, description) {
		console.log('Client has disconnected.');
		sockets = sockets.filter(socket => socket != connection)
	});
});


setInterval(() => {
	broadCast('ping');
}, 30000);

app.get('/output/enable/:status/', (req, res) => {
    var status = req.params.status;
    if (status == "on") {
        enable.write(1);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else if (status == "off") {
        enable.write(0);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else {
        res.status(500);
	    res.json({ "message": "Status incorrect" })
    } 
})

app.get('/output/direction/:status/', (req, res) => {
    var status = req.params.status;
    if (status == "on") {
        direction.write(1);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else if (status == "off") {
        direction.write(0);
        res.status(200);
        res.json({ "result": "ok" });
    }
    else {
        res.status(500);
	    res.json({ "message": "Status incorrect" })
    } 
})

app.get('/output/maxTorque/:value/', (req, res) => {
    var value = req.params.value;
    if (value == undefined || value < 0 || value > 255) {
        res.status(500);
        res.json({ "message": "Incorrect value" });
    }
    else {
        maxTorque.write(parseInt(value)/255)
        res.status(200);
        res.json({ "result": "ok" })
    } 
})

app.get('/output/maxSpeed/:value/', (req, res) => {
    var value = req.params.value;
    if (value == undefined || value < 0 || value > 255) {
        res.status(500);
        res.json({ "message": "Incorrect value" });
    }
    else {
        maxSpeed.write(parseInt(value)/255)
        res.status(200);
        res.json({ "result": "ok" })
    } 
})


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))