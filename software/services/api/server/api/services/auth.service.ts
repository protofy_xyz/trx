import L from '../../common/logger';
import UserModel from '../../../common/models/user';
import User from '../datamodels/User';
import settings from '../../settings';
const jwt = require('jsonwebtoken');

export class AuthService {
  login(email: string, password: string): Promise<UserModel> {
    L.info(`Trying to login with email: ${email}`);
    return new Promise((resolve, reject) => {
      User.findOne({ email }, function (err, user) {
        if (err) {
          L.info(err);
          return reject(new Error('Invalid credentials'));
        } else if (!user) {
          L.info('No such user');
          return reject(new Error('Invalid credentials'));
        } else {
          user.isCorrectPassword(password, function (err, valid) {
            if (err) {
              return reject(new Error('Invalid credentials'));
            } else if (!valid) {
              return reject(new Error('Invalid credentials'));
            } else {
              let token = "";
              if(user.status == "confirmed") {
                const payload = { name: user.name, email: user.email, type: user.type, stripeId: user.stripeId };
                token = jwt.sign(payload, settings.secret, { expiresIn: '100d' });
              }
              user.token = token;
              return resolve(UserModel.prototype.load(user));
            }
          });
        }
      });
    });
  }
}

export default new AuthService();
