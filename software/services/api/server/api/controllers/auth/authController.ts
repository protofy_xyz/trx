import { Request, Response } from 'express';
import authService from '../../services/auth.service';
import User from '../../../../common/models/user';

export class AuthController {
  login(req: Request, res: Response): void {
    authService.login(req.body.email, req.body.password).then((user: User) => {
      res.status(200).send(user);
    }).catch((reason: any) => {
      res.status(401).end();
    });
  }
}
export default new AuthController();
