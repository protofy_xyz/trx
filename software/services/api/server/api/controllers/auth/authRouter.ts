import express from 'express';
import authController from './authController';

export default express.Router()
  .post('/login', authController.login)
