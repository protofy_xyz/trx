#!/bin/sh
set -e
echo "Starting frontend version: $VERSION"

ROOT_DIR=public

if [ -z "$DEV_MODE" ]
then
      ROOT_DIR=build
fi


addEnvar () {
      if [ -z "$1" ]
      then
            echo "Error: addEnvar requires at least a variable name"
            exit
      fi

      VALUE=undefined\;
      if [ -n "$2" ]
      then
            VALUE=\'$2\'\;
      fi
      echo window.envars.$1 = $VALUE >> ${ROOT_DIR}/envars.js
}

cp public/_envars.js ${ROOT_DIR}/envars.js

addEnvar VERSION $VERSION
addEnvar PUBLIC_STRIPE_KEY $PUBLIC_STRIPE_KEY

if [ -z "$DEV_MODE" ]
then
      echo "Production mode enabled"
      serve -l 8100 -s build
else
      echo "Development mode enabled"
      yarn install
      CHOKIDAR_USEPOLLING=1 ionic serve --all
fi