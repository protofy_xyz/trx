import React  from 'react';
import { IonApp, IonRouterOutlet, IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Route} from 'react-router-dom';
import { setupConfig } from '@ionic/core';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
//import Login from './pages/Login';
import Home from './pages/Home';
import Modes from './pages/Modes';
import TrainMode from './pages/TrainMode';


import { Provider } from 'react-redux';
import ServiceApi from './api/ServiceApi';
import * as serviceWorker from './serviceWorker';
import ApiCaller from './lib/ApiCaller';
import Settings from './lib/settings';

setupConfig({
  mode: 'md'
});

interface ContainerProps  {
  store: any
}

const App: React.FC<ContainerProps> = ({store}) => {
  React.useEffect(() => {
      //check version
      let serviceApi = new ServiceApi(new ApiCaller());
      serviceApi.getVersion().then((data:any) => {
        if(data.version != Settings.getVersion()) {
          serviceWorker.unregister();
          document.location.reload(true);
        }
      });
  }, []);

  return (
    <Provider store={store}>
      <IonApp>
        <IonReactRouter>
          <IonSplitPane contentId="main">
            <IonRouterOutlet id="main">
              <Route path="/Home" component={Home} exact />
              <Route path="/" component={Modes} exact />  
              <Route path="/TrainMode" component={TrainMode} exact />            
            </IonRouterOutlet>
          </IonSplitPane>
        </IonReactRouter>  
      </IonApp>
    </Provider>
  );

};

export default App;
