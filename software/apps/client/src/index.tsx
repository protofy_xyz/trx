import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Settings from './lib/settings';
import configureStore from './store/store';
import { SessionState, updateSession } from './store/sessionStore';

const store = configureStore();

//Settings.getCurrentUser().then((userInfo) => {
   // const sessionData:SessionState = {
   //     loggedIn: userInfo.email ? true : false,
   //     ready: true,
   //    userInfo: userInfo
   // };

   // store.dispatch(updateSession(sessionData));

    ReactDOM.render(<App store={store} />, document.getElementById('root'));
//});



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
