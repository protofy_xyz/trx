import Storage from './storage';
//import User from './common/models/user';

//var currentUser: User;

/*var getCurrentUser = async (): Promise<User> => {
    var data = await Storage.read('user');
    currentUser = User.prototype.load(data);
    return currentUser;
}*/

const getEnv = (name: string, defaultValue: string): string => {
    if(!(window as any).envars) return defaultValue;
    return (window as any).envars[name] ? (window as any).envars[name] : defaultValue;
}

var Settings = {
    //currentUser: () : User => currentUser,

    //getCurrentUser: getCurrentUser,

    /*isLoggedIn: async (): Promise<boolean> => {
        try {
            var currentUser: User = await getCurrentUser();
            console.log('current user:', currentUser);
            return currentUser ? true : false;
        } catch (err) {
            return false;
        }
    },*/

    /*setCurrentUser: async (user: User): Promise<void> => {
        currentUser = user;
        return await Storage.write('user', user);
    },*/

    /*logout: async () => {
        return await Storage.clear('user');
    },*/

    getApiURL: (): string => {
        //return window.location.protocol + '//' + window.location.hostname + ':8080/api';
        return '/api';
    },

    getPublicURL: (): string => {
        return window.location.protocol + '//' + window.location.hostname+'/qr/';
    },

    getVersion: (): string => {
        return getEnv('VERSION', '1.0.0');
    }
}

export default Settings;
