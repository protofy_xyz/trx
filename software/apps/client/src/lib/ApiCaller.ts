import Settings from '../lib/settings';

class ApiCaller {
    token: string;
    constructor(token?:string) {
        this.token = token?token:"";
    }

    call(url:string, method:string, params?:object) : Promise<any>{
        var fetchParams:any = {
            method: method,
            headers: {
                'Content-Type': 'application/json'
            }
        }

        if(params) {
            fetchParams.body = JSON.stringify(params);
        }

        var defUrl = url + (this.token?"?token="+this.token:"");

        return fetch(Settings.getApiURL() +defUrl, fetchParams)
            .then(response => response.json());
    }
}

export default ApiCaller;