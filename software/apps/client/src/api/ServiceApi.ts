import ApiCaller from '../lib/ApiCaller';

class ServiceApi {
    apiCaller: ApiCaller;

    constructor(apiCaller:ApiCaller) {
        this.apiCaller = apiCaller;
    }


    getVersion() : Promise<string> {
        return this.apiCaller.call('/service/version', 'GET')
    }
}

export default ServiceApi;