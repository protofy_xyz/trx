import ApiCaller from '../lib/ApiCaller';
import { thermometerOutline } from 'ionicons/icons';

class AuthApi {
    apiCaller: ApiCaller;

    constructor(apiCaller:ApiCaller) {
        this.apiCaller = apiCaller;
    }

    login(email:string, password:string): Promise<void  | undefined> {
        return this.apiCaller.call('/auth/authenticate', 'POST', {email:email, password:password})
        .then(data => {
            if (data.error) {
                throw new Error("Server error");
            }
            return data;
        })
    }

    register(email:string, password:string, name:string): Promise<void  | undefined> {
        return this.apiCaller.call('/user/register', 'POST', {name:name, email:email, password:password})
        .then(data => {
            if (data.error) {
                throw new Error("Server error");
            }
            return data;
        })
    }

    generateQrAuth(): Promise<string> {
        return this.apiCaller.call('/auth/generateQrAuth', 'GET')
        .then(data => {
            if(data.error) {
                throw new Error("Server error");
            }
            return data;
        });
    }

    getTokenById(id: string): Promise<string> {
        return this.apiCaller.call('/auth/qrAuth/'+id, 'GET');
    }

    verify(email: string, token: string): Promise<void | undefined> {
        return this.apiCaller.call('/auth/verify/'+email+'/'+token, 'GET');
    }

    resetPassword(email: string): Promise<void | undefined> {
        return this.apiCaller.call('/auth/resetPassword/'+email, 'GET');
    }

    changePassword(email: string, password: string, token: string): Promise<void | undefined> {
        return this.apiCaller.call('/auth/resetPassword', 'POST', {email: email, password: password, token: token});
    }
}

export default AuthApi;