import { IonContent, IonPage, IonButton} from '@ionic/react';
import React from 'react';
import { Link } from 'react-router-dom';
import '../General.css';

const Home: React.FC = () => {

    return (
        <IonPage>
            <IonContent>    
                <br></br>
                <Link style={{ textDecoration: 'none' }} to='/TrainMode'>
                    <IonButton expand="full" type="button" shape="round" color="success" className="margins">
                        TRAIN MODE
                    </IonButton>
                </Link>
                <Link style={{ textDecoration: 'none' }} to='/'>
                    <IonButton expand="full" type="button" shape="round" color="success" className="margins" >
                        MODES
                    </IonButton>
                </Link>
            </IonContent>
        </IonPage>



    );
};

export default Home;