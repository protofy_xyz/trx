import { IonBackButton, IonText, IonContent, IonPage, IonImg, IonButton, IonGrid, IonRow, IonCol, IonList, IonItem, IonInput, IonItemDivider } from '@ionic/react';
import React from 'react';

import './styles/Login.css';
import Settings from '../lib/settings';
import { useHistory } from 'react-router-dom';
//import User from '../lib/common/models/user';
import AuthApi from '../api/AuthApi';
import ApiCaller from '../lib/ApiCaller';
import { SessionState,updateSession } from '../store/sessionStore';
import { useDispatch } from 'react-redux';


const Page: React.FC = () => {
    const dispatch = useDispatch();
    const inputs = ['email', 'password'];
    const history = useHistory();

    React.useEffect(() => {
        var timer: ReturnType<typeof setTimeout>;
        document.addEventListener('focusout', function(e:any) {
            if(e.target && inputs.indexOf(e.target.name) != -1) {
                let content = document.getElementById('loginContent');
                if(content) {
                    timer = setTimeout(() => {
                        // @ts-ignore
                        content.scrollToTop();
                    }, 1);
                }
            }

        });

        document.addEventListener('focusin', function(e:any) {
            if(e.target && inputs.indexOf(e.target.name) != -1) {
                // @ts-ignore
                clearTimeout(timer);
            }
        });
    }, []);

    const doAuth = (e: React.FormEvent) => {
        e.preventDefault();
        const email = (document.getElementById('email')?.children[0] as any)?.value;
        const password = (document.getElementById('password')?.children[0] as any)?.value;

        if(!email || !password) {
            return;
        }

        var authApi = new AuthApi(new ApiCaller());
        authApi.login(email, password).then((data:any) => {
            if(data.status != "confirmed") {
                history.push('/registered');
                return;
            }
  //          let user = new User(data.name, data.email, data.token, data.type);
  //          const sessionData:SessionState = {
  //              loggedIn: user.email? true : false,
  //              ready: true,
  //              userInfo: user
  //          };
  //          dispatch(updateSession(sessionData));

 //           Settings.setCurrentUser(user).then(() => {
 //              history.push("/"+data.type);
 //           });

        }).catch(err => {
            console.error(err);
            alert('Usuario o contraseña incorrectos');
        });
        e.preventDefault();
    }

    return (
    <IonPage>
        <IonContent id="loginContent" className="limited-center" scrollEvents={true} scrollY={false}>
            <form onSubmit={doAuth}>
                <IonGrid>
                    <IonRow>
                    <IonCol size="12">
                        <IonList lines="full">
                            <IonItem>
                                <IonInput id="email" name="email" type="email" required placeholder="Email"></IonInput>
                            </IonItem>
                            <IonItem>
                                <IonInput id="password" name="password" required type="password" placeholder="Password" clearInput></IonInput>
                            </IonItem>
                        </IonList>
                    </IonCol>
                    </IonRow>
                    <IonRow class="bottom-controls">
                        <IonCol size="12">
                            <IonButton type="submit" class="button-round" expand="full" shape="round">Confirma</IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
                <input type="submit" className="submit-enter" />
            </form>
            <div className="filler"></div>
        </IonContent>
    </IonPage>
    );
};

export default Page;
