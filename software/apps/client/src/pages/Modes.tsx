import { IonContent, IonPage, IonButton, IonSegment, IonLabel, IonRange, IonItem, IonList, IonIcon, IonCol, IonRow, IonSegmentButton, IonProgressBar } from '@ionic/react';
import React, { useState } from 'react';
import { addOutline, removeOutline } from 'ionicons/icons';
import '../General.css';
import { cleanup } from '@testing-library/react';
import { ifStatement, identifier } from '@babel/types';

var globalDurationTimeCounter = 0;
var durationTimeLeftCounter = 0;
var durationInterval: any;

var globalStartTimeCounter = 0;
var startTimeLeftCounter = 0;
var startTimeInterval: any;


const Modes: React.FC = (props: any) => {
  const [constSpeedSettings, setConstSpeedSettings] = useState<{
    startTime: number;
    duration: number;
    speed: number;
    direction: number;
  }>({
    startTime: 5,
    duration: 0,
    speed: 5,
    direction: 0
  });
  const [randomTurnsSettings, setRandomTurnsSettings] = useState<{
    startTime: number;
    duration: number;
    rangeSpeed: {
      lower: number;
      upper: number;
    };
    timeBetweenExercise: {
      lower: number;
      upper: number;
    };
  }>({
    startTime: 5,
    duration: 0,
    rangeSpeed: {
      lower: 5,
      upper: 100,
    },
    timeBetweenExercise: {
      lower: 3,
      upper: 30,
    }
  })
  const [randomTicksSettings, setRandomTicksSettings] = useState<{
    startTime: number;
    duration: number;
    rangeSpeed: {
      lower: number;
      upper: number;
    };
    timeBetweenExercise: {
      lower: number;
      upper: number;
    };
    exerciseDurationTime: {
      lower: number;
      upper: number;
    }
  }>({
    startTime: 5,
    duration: 0,
    rangeSpeed: {
      lower: 5,
      upper: 100,
    },
    timeBetweenExercise: {
      lower: 3,
      upper: 30,
    },
    exerciseDurationTime: {
      lower: 6,
      upper: 14
    }
  })
  const [isConstSpeedUnfolded, setConstSpeedUnfolded] = useState(false);
  const [isRandomTurnsUnfolded, setRandomTurnsUnfolded] = useState(false);
  const [isRandomTicksUnfolded, setRandomTicksUnfolded] = useState(false);
  const [isConstSpeedOn, setConstSpeedOn] = useState(false);
  const [isRandomTurnsOn, setRandomTurnsOn] = useState(false);
  const [isRandomTicksOn, setRandomTicksOn] = useState(false);
  const [durationProgress, setDurationProgress] = useState(0);
  const [startTimeProgress, setStartTimeProgress] = useState(0);
  const [direction, setDirection] = useState("left")
  const windowHostname = window.location.hostname;

  const enableConstantSpeed = () => {
    if (!isRandomTicksOn && !isRandomTurnsOn && !isConstSpeedOn) {
      setRandomTurnsUnfolded(false);
      setRandomTicksUnfolded(false);
      if (isConstSpeedUnfolded) setConstSpeedUnfolded(false);
      else setConstSpeedUnfolded(true);
    } else {
      alert("Exercise in progress")
    }
  }

  const enableRandomTurns = () => {
    if (!isRandomTicksOn && !isRandomTurnsOn && !isConstSpeedOn) {
      setConstSpeedUnfolded(false);
      setRandomTicksUnfolded(false);
      if (isRandomTurnsUnfolded) setRandomTurnsUnfolded(false);
      else setRandomTurnsUnfolded(true);
    } else {
      alert("Exercise in progress")
    }
  }

  const enableRandomTicks = () => {
    if (!isRandomTicksOn && !isRandomTurnsOn && !isConstSpeedOn) {
      setConstSpeedUnfolded(false);
      setRandomTurnsUnfolded(false);
      if (isRandomTicksUnfolded) setRandomTicksUnfolded(false);
      else setRandomTicksUnfolded(true);
    } else {
      alert("Exercise in progress")
    }
  }

  const knobChangeTimeBetweenExerciseTurns = (range: any) => {
    if (range.detail.value.lower !== randomTurnsSettings.timeBetweenExercise.lower) {
      setRandomTurnsSettings({
        startTime: randomTurnsSettings.startTime,
        duration: randomTurnsSettings.duration,
        rangeSpeed: {
          lower: randomTurnsSettings.rangeSpeed.lower,
          upper: randomTurnsSettings.rangeSpeed.upper,
        },
        timeBetweenExercise: {
          lower: range.detail.value.lower,
          upper: randomTurnsSettings.timeBetweenExercise.upper,
        }
      });
    } else if (range.detail.value.upper !== randomTurnsSettings.timeBetweenExercise.upper) {
      setRandomTurnsSettings({
        startTime: randomTurnsSettings.startTime,
        duration: randomTurnsSettings.duration,
        rangeSpeed: {
          lower: randomTurnsSettings.rangeSpeed.lower,
          upper: randomTurnsSettings.rangeSpeed.upper,
        },
        timeBetweenExercise: {
          lower: randomTurnsSettings.timeBetweenExercise.lower,
          upper: range.detail.value.upper,
        }
      });
    }
  }

  const knobChangeTimeBetweenExerciseTicks = (range: any) => {
    if (range.detail.value.lower !== randomTicksSettings.timeBetweenExercise.lower) {
      setRandomTicksSettings({
        startTime: randomTicksSettings.startTime,
        duration: randomTicksSettings.duration,
        rangeSpeed: {
          lower: randomTicksSettings.rangeSpeed.lower,
          upper: randomTicksSettings.rangeSpeed.upper,
        },
        timeBetweenExercise: {
          lower: range.detail.value.lower,
          upper: randomTicksSettings.timeBetweenExercise.upper,
        },
        exerciseDurationTime: {
          lower: randomTicksSettings.exerciseDurationTime.lower,
          upper: randomTicksSettings.exerciseDurationTime.upper,
        }
      });
    } else if (range.detail.value.upper !== randomTicksSettings.timeBetweenExercise.upper) {
      setRandomTicksSettings({
        startTime: randomTicksSettings.startTime,
        duration: randomTicksSettings.duration,
        rangeSpeed: {
          lower: randomTicksSettings.rangeSpeed.lower,
          upper: randomTicksSettings.rangeSpeed.upper,
        },
        timeBetweenExercise: {
          lower: randomTicksSettings.timeBetweenExercise.lower,
          upper: range.detail.value.upper,
        },
        exerciseDurationTime: {
          lower: randomTicksSettings.exerciseDurationTime.lower,
          upper: randomTicksSettings.exerciseDurationTime.upper,
        }
      });
    }
  }

  const knobChangeSpeedTurns = (range: any) => {
    if (range.detail.value.lower !== randomTurnsSettings.rangeSpeed.lower) {
      setRandomTurnsSettings({
        startTime: randomTurnsSettings.startTime,
        duration: randomTurnsSettings.duration,
        rangeSpeed: {
          lower: range.detail.value.lower,
          upper: randomTurnsSettings.rangeSpeed.upper,
        },
        timeBetweenExercise: {
          lower: randomTurnsSettings.timeBetweenExercise.lower,
          upper: randomTurnsSettings.timeBetweenExercise.upper,
        }
      });
    } else if (range.detail.value.upper !== randomTurnsSettings.rangeSpeed.upper) {
      setRandomTurnsSettings({
        startTime: randomTurnsSettings.startTime,
        duration: randomTurnsSettings.duration,
        rangeSpeed: {
          lower: randomTurnsSettings.rangeSpeed.lower,
          upper: range.detail.value.upper,
        },
        timeBetweenExercise: {
          lower: randomTurnsSettings.timeBetweenExercise.lower,
          upper: randomTurnsSettings.timeBetweenExercise.upper,
        }
      });
    }
  }

  const knobChangeSpeedTicks = (range: any) => {
    if (range.detail.value.lower !== randomTicksSettings.rangeSpeed.lower) {
      setRandomTicksSettings({
        startTime: randomTicksSettings.startTime,
        duration: randomTicksSettings.duration,
        rangeSpeed: {
          lower: range.detail.value.lower,
          upper: randomTicksSettings.rangeSpeed.upper,
        },
        timeBetweenExercise: {
          lower: randomTicksSettings.timeBetweenExercise.lower,
          upper: randomTicksSettings.timeBetweenExercise.upper,
        },
        exerciseDurationTime: {
          lower: randomTicksSettings.exerciseDurationTime.lower,
          upper: randomTicksSettings.exerciseDurationTime.upper,
        }
      });
    } else if (range.detail.value.upper !== randomTicksSettings.rangeSpeed.upper) {
      setRandomTicksSettings({
        startTime: randomTicksSettings.startTime,
        duration: randomTicksSettings.duration,
        rangeSpeed: {
          lower: randomTicksSettings.rangeSpeed.lower,
          upper: range.detail.value.upper,
        },
        timeBetweenExercise: {
          lower: randomTicksSettings.timeBetweenExercise.lower,
          upper: randomTicksSettings.timeBetweenExercise.upper,
        },
        exerciseDurationTime: {
          lower: randomTicksSettings.exerciseDurationTime.lower,
          upper: randomTicksSettings.exerciseDurationTime.upper,
        }
      });
    }
  }

  const knobChangeExerciseDurationTime = (range: any) => {
    if (range.detail.value.lower !== randomTicksSettings.exerciseDurationTime.lower) {
      setRandomTicksSettings({
        startTime: randomTicksSettings.startTime,
        duration: randomTicksSettings.duration,
        rangeSpeed: {
          lower: randomTicksSettings.rangeSpeed.lower,
          upper: randomTicksSettings.rangeSpeed.upper,
        },
        timeBetweenExercise: {
          lower: randomTicksSettings.timeBetweenExercise.lower,
          upper: randomTicksSettings.timeBetweenExercise.upper,
        },
        exerciseDurationTime: {
          lower: range.detail.value.lower,
          upper: randomTicksSettings.exerciseDurationTime.upper,
        }
      });
    } else if (range.detail.value.upper !== randomTicksSettings.exerciseDurationTime.upper) {
      setRandomTicksSettings({
        startTime: randomTicksSettings.startTime,
        duration: randomTicksSettings.duration,
        rangeSpeed: {
          lower: randomTicksSettings.rangeSpeed.lower,
          upper: randomTicksSettings.rangeSpeed.upper,
        },
        timeBetweenExercise: {
          lower: randomTicksSettings.timeBetweenExercise.lower,
          upper: randomTicksSettings.timeBetweenExercise.upper,
        },
        exerciseDurationTime: {
          lower: randomTicksSettings.exerciseDurationTime.lower,
          upper: range.detail.value.upper,
        }
      });
    }
  }

  const constantSpeedOn = () => {
    if (constSpeedSettings.startTime === 0) setStartTimeProgress(1);
    if (constSpeedSettings.duration === 0) setDurationProgress(1);
    setConstSpeedOn(true);
    progressStartTimeBar(constSpeedSettings.startTime);
  }

  const randomTurnsOn = () => {
    if (randomTurnsSettings.startTime === 0) setStartTimeProgress(1);
    if (randomTurnsSettings.duration === 0) setDurationProgress(1);
    setRandomTurnsOn(true);
    progressStartTimeBar(randomTurnsSettings.startTime);
  }

  const randomTicksOn = () => {
    if (randomTicksSettings.startTime === 0) setStartTimeProgress(1);
    if (randomTicksSettings.duration === 0) setDurationProgress(1);
    setRandomTicksOn(true);
    progressStartTimeBar(randomTicksSettings.startTime);
  }

  const stop = () => {
    console.log("stopping")
    fetch('http://' + windowHostname + ':4000/output/enable/off/', { method: 'GET' })
      .then(response => response.json())
      .catch(error => console.log(error));

    setDefaultValues();
  }

  const setDefaultValues = () => {
    setConstSpeedSettings({ startTime: 5, duration: 0, speed: 5, direction: 0 })
    setRandomTurnsSettings({ startTime: 5, duration: 0, rangeSpeed: { lower: 5, upper: 100, }, timeBetweenExercise: { lower: 3, upper: 30, } })
    setRandomTicksSettings({ startTime: 5, duration: 0, rangeSpeed: { lower: 5, upper: 100, }, timeBetweenExercise: { lower: 3, upper: 30, }, exerciseDurationTime: { lower: 6, upper: 14 } })
    setConstSpeedOn(false);
    setRandomTurnsOn(false);
    setRandomTicksOn(false);
    setDurationProgress(0);
    setDirection("left");
    clearInterval(durationInterval);
    globalDurationTimeCounter = 0;
    durationTimeLeftCounter = 0;
    setStartTimeProgress(0);
    clearInterval(startTimeInterval);
    globalStartTimeCounter = 0;
    startTimeLeftCounter = 0;
  }

  const progressDurationBar = (duration: number) => {
    durationInterval = setInterval(() => {
      if (globalDurationTimeCounter >= 1) {
        return;
      }
      if (durationProgress === 0) {
        durationTimeLeftCounter = durationTimeLeftCounter + 1;
        globalDurationTimeCounter = globalDurationTimeCounter + 1 / duration;
        setDurationProgress(globalDurationTimeCounter);
      }
      if (durationTimeLeftCounter == duration) {
        clearInterval(durationInterval);
        stop();
      }
    }, 1000);
  }

  const startExercise = (mode: number) => {
    switch (mode) {
      case 1:
        console.log('http://' + windowHostname + ':4000/modes/constantSpeed/' + Math.floor(constSpeedSettings.speed * (255 / 100)) + '/' + constSpeedSettings.direction + '/' + constSpeedSettings.duration + '/')
        fetch('http://' + windowHostname + ':4000/modes/constantSpeed/' + Math.floor(constSpeedSettings.speed * (255 / 100)) + '/' + constSpeedSettings.direction + '/' + constSpeedSettings.duration + '/', { method: 'GET' })
          .then((response) => {
            response.json();
            progressDurationBar(constSpeedSettings.duration);
          })
          .catch((error) => {
            console.log(error);
            alert("It has not been possible to start the excercise");
            stop();
          });
        break;
      case 2:
        console.log('http://' + windowHostname + ':4000/modes/randomTurns/' + Math.floor(randomTurnsSettings.rangeSpeed.lower * (255 / 100)) + '/' + Math.floor(randomTurnsSettings.rangeSpeed.upper * (255 / 100)) + '/' + randomTurnsSettings.timeBetweenExercise.lower + '/' + randomTurnsSettings.timeBetweenExercise.upper + '/' + randomTurnsSettings.duration + '/');
        fetch('http://' + windowHostname + ':4000/modes/randomTurns/' + Math.floor(randomTurnsSettings.rangeSpeed.lower * (255 / 100)) + '/' + Math.floor(randomTurnsSettings.rangeSpeed.upper * (255 / 100)) + '/' + randomTurnsSettings.timeBetweenExercise.lower + '/' + randomTurnsSettings.timeBetweenExercise.upper + '/' + randomTurnsSettings.duration + '/', { method: 'GET' })
          .then((response) => {
            response.json();
            progressDurationBar(randomTurnsSettings.duration);
          })
          .catch((error) => {
            console.log(error);
            stop();
          });
        break;
      case 3:
        console.log('http://' + windowHostname + ':4000/modes/randomTicks/' + Math.floor(randomTicksSettings.rangeSpeed.lower * (255 / 100)) + '/' + Math.floor(randomTicksSettings.rangeSpeed.upper * (255 / 100)) + '/' + randomTicksSettings.timeBetweenExercise.lower + '/' + randomTicksSettings.timeBetweenExercise.upper + '/' + (randomTicksSettings.exerciseDurationTime.lower) / 10 + '/' + (randomTicksSettings.exerciseDurationTime.upper) / 10 + '/' + randomTicksSettings.duration + '/');
        fetch('http://' + windowHostname + ':4000/modes/randomTicks/' + Math.floor(randomTicksSettings.rangeSpeed.lower * (255 / 100)) + '/' + Math.floor(randomTicksSettings.rangeSpeed.upper * (255 / 100)) + '/' + randomTicksSettings.timeBetweenExercise.lower + '/' + randomTicksSettings.timeBetweenExercise.upper + '/' + (randomTicksSettings.exerciseDurationTime.lower) / 10 + '/' + (randomTicksSettings.exerciseDurationTime.upper) / 10 + '/' + randomTicksSettings.duration + '/', { method: 'GET' })
          .then((response) => {
            response.json();
            progressDurationBar(randomTicksSettings.duration);
          })
          .catch((error) => {
            console.log(error);
            stop();
          });
        break;
    }
  }

  const progressStartTimeBar = (startTime: number) => {
    startTimeInterval = setInterval(() => {
      if (globalStartTimeCounter >= 1) {
        return;
      }
      if (startTimeProgress === 0) {
        startTimeLeftCounter = startTimeLeftCounter + 1;
        globalStartTimeCounter = globalStartTimeCounter + 1 / startTime;
        setStartTimeProgress(globalStartTimeCounter);
      }
      if (startTimeLeftCounter == startTime) {
        clearInterval(startTimeInterval);
        if (isConstSpeedUnfolded) {
          startExercise(1);
        }
        if (isRandomTurnsUnfolded) {
          startExercise(2);
        }
        if (isRandomTicksUnfolded) {
          startExercise(3);
        }
      }
    }, 1000);
  }

  return (
    <IonPage>
      <IonContent>
        <IonList>
          <IonItem className="resize" lines="none">
            <IonCol>
              <IonRow className="titleSquare" onClick={enableConstantSpeed}>
                <IonCol><div className="title"> Constant Speed </div></IonCol>
                <IonCol><IonIcon size="small" icon={isConstSpeedUnfolded ? removeOutline : addOutline} className="flexRight" /></IonCol>
              </IonRow>
              {
                isConstSpeedUnfolded
                  ?
                  <IonItem lines="none">
                    {
                      !isConstSpeedOn
                        ? <IonItem lines="none" className="resize">
                          <IonCol>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Direction: </span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <div className="centered">
                                <IonSegment disabled={isConstSpeedOn} value={direction}>
                                  <IonSegmentButton className="ionSegmentButton" value="left"
                                    onClick={() => {
                                      setConstSpeedSettings({
                                        startTime: constSpeedSettings.startTime,
                                        duration: constSpeedSettings.duration,
                                        speed: constSpeedSettings.speed,
                                        direction: 0,
                                      });
                                      setDirection("left");
                                    }}>
                                    <IonLabel><span className="ionSegmentText">Left</span></IonLabel>
                                  </IonSegmentButton>
                                  <IonSegmentButton className="ionSegmentButton" value="right"
                                    onClick={() => {
                                      setConstSpeedSettings({
                                        startTime: constSpeedSettings.startTime,
                                        duration: constSpeedSettings.duration,
                                        speed: constSpeedSettings.speed,
                                        direction: 1,
                                      });
                                      setDirection("right");
                                    }}>
                                    <IonLabel><span className="ionSegmentText">Right</span></IonLabel>
                                  </IonSegmentButton>
                                </IonSegment>
                              </div>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Speed: </span><span className="tab"> {constSpeedSettings.speed}</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isConstSpeedOn} pin={false} value={constSpeedSettings.speed} min={5} max={100}
                                onIonChange={e => setConstSpeedSettings({
                                  startTime: constSpeedSettings.startTime,
                                  duration: constSpeedSettings.duration,
                                  speed: e.detail.value as number,
                                  direction: constSpeedSettings.direction
                                })}>
                                <IonLabel slot="start">5</IonLabel>
                                <IonLabel slot="end">100</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Exercise duration: </span><span className="tab"> {constSpeedSettings.duration} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange pin={false} value={constSpeedSettings.duration} max={60}
                                onIonChange={e => setConstSpeedSettings({
                                  startTime: constSpeedSettings.startTime,
                                  duration: e.detail.value as number,
                                  speed: constSpeedSettings.speed,
                                  direction: constSpeedSettings.direction
                                })}>
                                <IonLabel slot="start">0</IonLabel>
                                <IonLabel slot="end">60</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Time before exercise: </span><span className="tab"> {constSpeedSettings.startTime} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange pin={false} value={constSpeedSettings.startTime} min={5} max={30}
                                onIonChange={e => setConstSpeedSettings({
                                  startTime: e.detail.value as number,
                                  duration: constSpeedSettings.duration,
                                  speed: constSpeedSettings.speed,
                                  direction: constSpeedSettings.direction
                                })}>
                                <IonLabel slot="start">5</IonLabel>
                                <IonLabel slot="end">30</IonLabel>
                              </IonRange>
                            </IonRow>

                            <IonButton type="button" shape="round" color="success" expand="full" onClick={constantSpeedOn}><span className="button">ON</span></IonButton>

                          </IonCol>
                        </IonItem>
                        : <IonItem lines="none" className="resize">
                          <IonCol>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Direction: </span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <div className="centered">
                                <IonSegment disabled={isConstSpeedOn}>
                                  <IonSegmentButton className="ionSegmentButton" value="left"
                                    onClick={() => {
                                      setConstSpeedSettings({
                                        startTime: constSpeedSettings.startTime,
                                        duration: constSpeedSettings.duration,
                                        speed: constSpeedSettings.speed,
                                        direction: 0,
                                      });
                                    }}>
                                    <IonLabel><span className="ionSegmentText">Left</span></IonLabel>
                                  </IonSegmentButton>
                                  <IonSegmentButton className="ionSegmentButton" value="right"
                                    onClick={() => {
                                      setConstSpeedSettings({
                                        startTime: constSpeedSettings.startTime,
                                        duration: constSpeedSettings.duration,
                                        speed: constSpeedSettings.speed,
                                        direction: 1,
                                      });
                                    }}>
                                    <IonLabel><span className="ionSegmentText">Right</span></IonLabel>
                                  </IonSegmentButton>
                                </IonSegment>
                              </div>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Speed: </span><span className="tab"> {constSpeedSettings.speed}</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isConstSpeedOn} pin={false} value={constSpeedSettings.speed} min={5} max={100} onIonChange={e => setConstSpeedSettings({
                                startTime: constSpeedSettings.startTime,
                                duration: constSpeedSettings.duration,
                                speed: e.detail.value as number,
                                direction: constSpeedSettings.direction
                              })}>
                                <IonLabel slot="start">5</IonLabel>
                                <IonLabel slot="end">100</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Time left: </span><span className="tab"> {constSpeedSettings.duration - durationTimeLeftCounter} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonProgressBar value={durationProgress} className="ionProgressBar"></IonProgressBar>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle"> Starting in: </span><span className="tab"> {constSpeedSettings.startTime - startTimeLeftCounter} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonProgressBar value={startTimeProgress} className="ionProgressBar"></IonProgressBar>
                            </IonRow>

                            <br></br>
                            <IonButton type="button" shape="round" color="danger" expand="full" onClick={stop}><span className="button">STOP</span></IonButton>

                          </IonCol>
                        </IonItem>
                    }
                  </IonItem>
                  : <p></p>
              }
            </IonCol>
          </IonItem>
          <IonItem className="resize" lines="none">
            <IonCol>
              <IonRow className="titleSquare" onClick={enableRandomTurns}>
                <IonCol><div className="title">Random Turns </div></IonCol>
                <IonCol><IonIcon size="small" icon={isRandomTurnsUnfolded ? removeOutline : addOutline} className="flexRight" /> </IonCol>
              </IonRow>

              {
                isRandomTurnsUnfolded
                  ?
                  <IonItem lines="none">
                    {
                      !isRandomTurnsOn
                        ? <IonItem lines="none" className="resize">
                          <IonCol>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Speed: </span><span className="tab"> {randomTurnsSettings.rangeSpeed.lower} to {randomTurnsSettings.rangeSpeed.upper}</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isRandomTurnsOn} pin={false} value={randomTurnsSettings.rangeSpeed} min={5} max={100} dualKnobs={true} onIonChange={e => { knobChangeSpeedTurns(e); }}>
                                <IonLabel slot="start">5</IonLabel>
                                <IonLabel slot="end">100</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Inter turn time: </span><span className="tab"> {randomTurnsSettings.timeBetweenExercise.lower} to {randomTurnsSettings.timeBetweenExercise.upper} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isRandomTurnsOn} pin={false} value={randomTurnsSettings.timeBetweenExercise} dualKnobs={true} min={3} max={30} onIonChange={e => { knobChangeTimeBetweenExerciseTurns(e); }}>
                                <IonLabel slot="start">3</IonLabel>
                                <IonLabel slot="end">30</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Exercise duration: </span><span className="tab"> {randomTurnsSettings.duration} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange pin={false} value={randomTurnsSettings.duration} max={60}
                                onIonChange={e => setRandomTurnsSettings({
                                  startTime: randomTurnsSettings.startTime,
                                  duration: e.detail.value as number,
                                  rangeSpeed: {
                                    lower: randomTurnsSettings.rangeSpeed.lower,
                                    upper: randomTurnsSettings.rangeSpeed.upper,
                                  },
                                  timeBetweenExercise: {
                                    lower: randomTurnsSettings.timeBetweenExercise.lower,
                                    upper: randomTurnsSettings.timeBetweenExercise.upper,
                                  }
                                })}>
                                <IonLabel slot="start">0</IonLabel>
                                <IonLabel slot="end">60</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Time before exercise: </span><span className="tab"> {randomTurnsSettings.startTime} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange pin={false} value={randomTurnsSettings.startTime} min={5} max={30}
                                onIonChange={e => setRandomTurnsSettings({
                                  startTime: e.detail.value as number,
                                  duration: randomTurnsSettings.duration,
                                  rangeSpeed: {
                                    lower: randomTurnsSettings.rangeSpeed.lower,
                                    upper: randomTurnsSettings.rangeSpeed.upper,
                                  },
                                  timeBetweenExercise: {
                                    lower: randomTurnsSettings.timeBetweenExercise.lower,
                                    upper: randomTurnsSettings.timeBetweenExercise.upper,
                                  }
                                })}>
                                <IonLabel slot="start">5</IonLabel>
                                <IonLabel slot="end">30</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonButton type="button" shape="round" color="success" expand="full" onClick={randomTurnsOn}> <span className="button">ON</span></IonButton>
                          </IonCol>
                        </IonItem>
                        : <IonItem lines="none" className="resize">
                          <IonCol>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Speed: </span><span className="tab"> {randomTurnsSettings.rangeSpeed.lower} to {randomTurnsSettings.rangeSpeed.upper}</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isRandomTurnsOn} pin={false} value={randomTurnsSettings.rangeSpeed} min={5} max={100} dualKnobs={true} onIonChange={e => { knobChangeSpeedTurns(e); }}>
                                <IonLabel slot="start">5</IonLabel>
                                <IonLabel slot="end">100</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Inter turn time: </span><span className="tab"> {randomTurnsSettings.timeBetweenExercise.lower} to {randomTurnsSettings.timeBetweenExercise.upper} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isRandomTurnsOn} pin={false} value={randomTurnsSettings.timeBetweenExercise} dualKnobs={true} min={3} max={30} onIonChange={e => { knobChangeTimeBetweenExerciseTurns(e); }}>
                                <IonLabel slot="start">3</IonLabel>
                                <IonLabel slot="end">30</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Time left: </span><span className="tab"> {randomTurnsSettings.duration - durationTimeLeftCounter} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonProgressBar value={durationProgress} className="ionProgressBar"></IonProgressBar>
                            </IonRow>

                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle"> Starting in: </span><span className="tab"> {randomTurnsSettings.startTime - startTimeLeftCounter} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonProgressBar value={startTimeProgress} className="ionProgressBar"></IonProgressBar>
                            </IonRow>

                            <br></br>
                            <IonButton type="button" shape="round" color="danger" expand="full" onClick={stop}><span className="button">STOP</span></IonButton>
                          </IonCol>
                        </IonItem>
                    }
                  </IonItem>
                  : <p></p>
              }
            </IonCol>
          </IonItem>
          <IonItem className="resize" lines="none">
            <IonCol>
              <IonRow className="titleSquare" onClick={enableRandomTicks}>
                <IonCol><div className="title">Random Ticks </div></IonCol>
                <IonCol><IonIcon size="small" icon={isRandomTicksUnfolded ? removeOutline : addOutline} className="flexRight" /> </IonCol>
              </IonRow>
              {
                isRandomTicksUnfolded
                  ?
                  <IonItem lines="none" className="resize">
                    {
                      !isRandomTicksOn
                        ? <IonItem lines="none" className="resize">
                          <IonCol>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Speed: </span><span className="tab"> {randomTicksSettings.rangeSpeed.lower} to {randomTicksSettings.rangeSpeed.upper}</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isRandomTicksOn} pin={false} value={randomTicksSettings.rangeSpeed} min={5} max={100} dualKnobs={true} onIonChange={e => { knobChangeSpeedTicks(e); }}>
                                <IonLabel slot="start">5</IonLabel>
                                <IonLabel slot="end">100</IonLabel>
                              </IonRange>
                            </IonRow>

                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Inter tick time: </span><span className="tab"> {randomTicksSettings.timeBetweenExercise.lower} to {randomTicksSettings.timeBetweenExercise.upper} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isRandomTicksOn} pin={false} value={randomTicksSettings.timeBetweenExercise} min={3} max={30} dualKnobs={true} onIonChange={e => { knobChangeTimeBetweenExerciseTicks(e); }}>
                                <IonLabel slot="start">3</IonLabel>
                                <IonLabel slot="end">30</IonLabel>
                              </IonRange>
                            </IonRow>

                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Tick duration: </span><span className="tab"> {randomTicksSettings.exerciseDurationTime.lower / 10} to {randomTicksSettings.exerciseDurationTime.upper / 10} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isRandomTicksOn} pin={false} value={randomTicksSettings.exerciseDurationTime} min={6} max={14} dualKnobs={true} className="moveLeft" onIonChange={e => { knobChangeExerciseDurationTime(e); }}>
                                <IonLabel slot="start">{randomTicksSettings.exerciseDurationTime.lower / 10}</IonLabel>
                                <IonLabel slot="end">{randomTicksSettings.exerciseDurationTime.upper / 10}</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Exercise duration: </span><span className="tab"> {randomTicksSettings.duration} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange pin={false} value={randomTicksSettings.duration} max={60} onIonChange={e => setRandomTicksSettings({
                                startTime: randomTicksSettings.startTime,
                                duration: e.detail.value as number,
                                rangeSpeed: {
                                  lower: randomTicksSettings.rangeSpeed.lower,
                                  upper: randomTicksSettings.rangeSpeed.upper,
                                },
                                timeBetweenExercise: {
                                  lower: randomTicksSettings.timeBetweenExercise.lower,
                                  upper: randomTicksSettings.timeBetweenExercise.upper,
                                },
                                exerciseDurationTime: {
                                  lower: randomTicksSettings.exerciseDurationTime.lower,
                                  upper: randomTicksSettings.exerciseDurationTime.upper,
                                }
                              })}>
                                <IonLabel slot="start">0</IonLabel>
                                <IonLabel slot="end">60</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Time before exercise: </span><span className="tab"> {randomTicksSettings.startTime} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange pin={false} value={randomTicksSettings.startTime} min={5} max={30}
                                onIonChange={e => setRandomTicksSettings({
                                  startTime: e.detail.value as number,
                                  duration: randomTicksSettings.duration,
                                  rangeSpeed: {
                                    lower: randomTicksSettings.rangeSpeed.lower,
                                    upper: randomTicksSettings.rangeSpeed.upper,
                                  },
                                  timeBetweenExercise: {
                                    lower: randomTicksSettings.timeBetweenExercise.lower,
                                    upper: randomTicksSettings.timeBetweenExercise.upper,
                                  },
                                  exerciseDurationTime: {
                                    lower: randomTicksSettings.exerciseDurationTime.lower,
                                    upper: randomTicksSettings.exerciseDurationTime.upper,
                                  }
                                })}>
                                <IonLabel slot="start">5</IonLabel>
                                <IonLabel slot="end">30</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonButton type="button" shape="round" color="success" expand="full" onClick={randomTicksOn}><span className="button">ON</span></IonButton>

                          </IonCol>
                        </IonItem>
                        : <IonItem lines="none" className="resize">
                          <IonCol>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Speed: </span><span className="tab"> {randomTicksSettings.rangeSpeed.lower} to {randomTicksSettings.rangeSpeed.upper}</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isRandomTicksOn} pin={false} value={randomTicksSettings.rangeSpeed} min={5} max={100} dualKnobs={true} onIonChange={e => { knobChangeSpeedTicks(e); }}>
                                <IonLabel slot="start">5</IonLabel>
                                <IonLabel slot="end">100</IonLabel>
                              </IonRange>
                            </IonRow>

                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Inter tick time: </span><span className="tab"> {randomTicksSettings.timeBetweenExercise.lower} to {randomTicksSettings.timeBetweenExercise.upper} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange disabled={isRandomTicksOn} pin={false} value={randomTicksSettings.timeBetweenExercise} min={3} max={30} dualKnobs={true} onIonChange={e => { knobChangeTimeBetweenExerciseTicks(e); }}>
                                <IonLabel slot="start">3</IonLabel>
                                <IonLabel slot="end">30</IonLabel>
                              </IonRange>
                            </IonRow>

                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Exercise time: </span><span className="tab"> {randomTicksSettings.exerciseDurationTime.lower / 10} to {randomTicksSettings.exerciseDurationTime.upper / 10} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonRange className="moveLeft" disabled={isRandomTicksOn} pin={false} value={randomTicksSettings.exerciseDurationTime} min={6} max={14} dualKnobs={true} onIonChange={e => { knobChangeExerciseDurationTime(e); }}>
                                <IonLabel slot="start">{randomTicksSettings.exerciseDurationTime.lower / 10}</IonLabel>
                                <IonLabel slot="end">{randomTicksSettings.exerciseDurationTime.upper / 10}</IonLabel>
                              </IonRange>
                            </IonRow>
                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle">Time left: </span><span className="tab"> {randomTicksSettings.duration - durationTimeLeftCounter} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonProgressBar value={durationProgress} className="ionProgressBar"></IonProgressBar>
                            </IonRow>

                            <IonRow>
                              <div className="margins">
                                <span className="smallTitle"> Starting in: </span><span className="tab"> {randomTicksSettings.startTime - startTimeLeftCounter} sec</span>
                              </div>
                            </IonRow>
                            <IonRow>
                              <IonProgressBar value={startTimeProgress} className="ionProgressBar"></IonProgressBar>
                            </IonRow>
                            <br></br>
                            <IonButton type="button" shape="round" color="danger" expand="full" onClick={stop}><span className="button">STOP</span></IonButton>

                          </IonCol>
                        </IonItem>
                    }
                  </IonItem>
                  : <p></p>

              }
            </IonCol>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>



  );
};

export default Modes;