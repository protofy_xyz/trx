import { IonContent, IonRange, IonPage, IonButton, IonItem, IonLabel} from '@ionic/react';
import React, { useState } from 'react';
import { Link} from 'react-router-dom';
import '../General.css';

const TrainMode: React.FC = (props:any) => {
  
  //const currentUser = useSelector((state: AppState) => state.session.userInfo);
  const [torque, setTorque] = useState(0);
  const [speed, setSpeed] = useState(0);
  const [isEnabled, setIsEnabled] = useState(0);
  const [isDirectioned, setIsDirectioned] = useState(0);
  const [rotation,isRotation]=useState(0);

  const windowHostname = window.location.hostname;

  React.useEffect(() => {
    let ws = new WebSocket('ws://localhost:9898/')
    ws.onerror = function () {
      console.log("error")
    };

    ws.onclose = function () {
      console.log("onclose")
    };

    ws.onopen = function () {
      console.log('WebSocket Client Connected');
    };

    ws.onmessage = function (e: any) {
     
      if(e.data === "ping") return;

      var parts = e.data.split(' ');
      var pin = parts[0];
      var value = parts[1];

      if(pin==="RotationDirection:"){
          isRotation(value);
      }
    };
  }, []);

  
  const enableButton = () => {
    if (isEnabled === 0) {
      fetch('http://' + windowHostname + ':4000/output/enable/on/', { method: 'GET' })
        .then(response => response.json())
        .catch(error => console.log(error));
      setIsEnabled(1);
    } else {
      fetch('http://' + windowHostname + ':4000/output/enable/off/', { method: 'GET' })
        .then(response => response.json())
        .catch(error => console.log(error));
      setIsEnabled(0);
    }
  }

  const directionButton = () => {
    if (isDirectioned === 0) {
      fetch('http://' + windowHostname + ':4000/output/direction/on/', { method: 'GET' })
        .then(response => response.json())
        .catch(error => console.log(error));
      setIsDirectioned(1);
    } else {
      fetch('http://' + windowHostname + ':4000/output/direction/off/', { method: 'GET' })
        .then(response => response.json())
        .catch(error => console.log(error));
      setIsDirectioned(0);
    }
  }

  const getRotation = () => {
    return rotation;
  }

  const getAnalog = () => {
    return "3";

  }

  const changeTorque = (torque: number) => {
    fetch('http://' + windowHostname + ':4000/output/torque/' + (torque * 255 / 100).toFixed().toString() + '/', { method: 'GET' })
      .then(response => response.json())
      .catch(error => console.log(error));
    setTorque(torque);
  }

  const changeSpeed = (speed: number) => {
    fetch('http://' + windowHostname + ':4000/output/speed/' + (speed * 255 / 100).toFixed().toString() + '/', { method: 'GET' })
      .then(response => response.json())
      .catch(error => console.log(error));
    setSpeed(speed);
  }
  return (
    <IonPage>
      <IonContent scrollY={false}>
        OUTPUTS :
        
      <IonItem lines="none">
          <IonButton type="submit" color="success" expand="full" shape="round" onClick={enableButton}>ENABLE</IonButton>
        </IonItem>
        <IonItem lines="none">
          <IonButton type="submit" color="success" expand="full" shape="round" onClick={directionButton}>DIRECTION</IonButton>
        </IonItem>
        <IonItem lines="none">
          TORQUE
        <IonRange pin={true} value={torque} onIonChange={e => changeTorque(e.detail.value as number)} />
        </IonItem>
        <IonItem lines="none">
          SPEED
        <IonRange pin={true} value={speed} onIonChange={e => changeSpeed(e.detail.value as number)} />
        </IonItem>
        <br></br>
      INPUTS :
      <IonItem lines="none">
          <IonLabel>Direction: {getRotation()}</IonLabel>
        </IonItem>
        <IonItem lines="none">
          <IonLabel>Analog:  {getAnalog()}</IonLabel>
        </IonItem>

        <Link style={{ textDecoration: 'none' }} to="/">
          <IonButton type="button" shape="round"  expand="full"  className="modeButton">BACK</IonButton>
        </Link>
      </IonContent>
    </IonPage>
  );
};

export default TrainMode;
