/*

This file is a template to be filled by start.sh when running in production mode.
start.sh will fil window.process with environment variables, so envars can be set on runtime
CRA by default only allows envars to be passed in build time

There is a conditional render in index.html including this file, only when in production mode. 
This file is copied to envars.js on runtime, by start.sh

*/

window.envars = {
  
};
