## Trx apps folder (frontend) introduction 

 - The main file of this frontend folder is **Modes.tsx**, from where you can configurate all the modes and start or stop them. In order to compile and execute this folder you have to run the following terminal scripts from the client folder: first *npm run build* to compile and *npm run start* to execute and start running the app.