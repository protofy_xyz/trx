#include "Arduino.h"
#include "Class_Motor_TRX.h"


Motor_TRX::Motor_TRX() {
}

void Motor_TRX::configurePins(int pin_on_off, int pin_direction_in, int pin_torque_in, int pin_torque_in_0, int pin_speed_in, int pin_speed_in_0, int pin_pulse_out, int pin_torque_out, int pin_direction_out) {

  _pin_on_off         = pin_on_off;
  _pin_direction_in   = pin_direction_in;
  _pin_torque_in      = pin_torque_in;
  _pin_torque_in_0    = pin_torque_in_0;
  _pin_speed_in       = pin_speed_in;
  _pin_speed_in_0     = pin_speed_in_0;
  _pin_pulse_out      = pin_pulse_out;
  _pin_torque_out     = pin_torque_out;
  _pin_direction_out  = pin_direction_out;

  pinMode(_pin_on_off, OUTPUT);
  pinMode(_pin_direction_in, OUTPUT);
  pinMode(_pin_torque_in, OUTPUT);
  pinMode(_pin_torque_in_0, OUTPUT);
  pinMode(_pin_speed_in, OUTPUT);
  pinMode(_pin_speed_in_0, OUTPUT);
  pinMode(_pin_pulse_out, INPUT);
  pinMode(_pin_torque_out, INPUT);
  pinMode(_pin_direction_out, INPUT);
}

void Motor_TRX::setDynamicTorque(bool _DYNAMIC_TORQUE) {
  DYNAMIC_TORQUE = _DYNAMIC_TORQUE;
}

void Motor_TRX::setSpeedThereshold(int speed_thereshold) {
  _speed_thereshold = speed_thereshold;
}

void Motor_TRX::debug(bool TRX_DEBUG) {
  if (TRX_DEBUG && millis() >= _debug_print_previous + _debug_print_period) {
    _debug_print_previous = millis();

    Serial.print("Enable ");
    Serial.print(w_enable);

    Serial.print("   W_Dir: ");
    Serial.print(w_direction);

    Serial.print("   R_Dir: ");
    Serial.print(r_direction);

    Serial.print("   W_Speed: ");
    Serial.print(w_speed);

    Serial.print("   R_Speed: ");
    Serial.print(r_speed);

    Serial.print("   W_Torque: ");
    Serial.print(w_torque);

    Serial.print("   R_Torque: ");
    Serial.println(r_torque);
  }
}

bool Motor_TRX::readEncoder() {
  if (newPulse) {
    newPulse = false;
    pulse_count = 0;
    _previous_pulse = _last_pulse;
    _last_pulse = millis();
    r_speed = 60 / (_last_pulse - _previous_pulse) * 24;
    return true;
  } else {
    return false;
  }
}

int Motor_TRX::readSpeed() {
  return r_speed;
}

int Motor_TRX::readTorque() {
  r_torque = digitalRead(_pin_torque_out);
  return r_torque;
}

bool Motor_TRX::readDirection() {
  r_direction = digitalRead(_pin_direction_out);
  return r_direction;
}

void Motor_TRX::writeEnable(bool _ENABLE) {
  w_enable = _ENABLE;
  digitalWrite(_pin_on_off, w_enable);
}

void Motor_TRX::writeDirection(bool _DIRECTION) {
  if (DYNAMIC_TORQUE) {
    Motor_TRX::dynamicTorque(w_speed, _DIRECTION);
  }
  w_direction = _DIRECTION;
  digitalWrite(_pin_direction_in, w_direction);
}

void Motor_TRX::writeTorque(int _TORQUE) {
  w_torque = _TORQUE;
  analogWrite(_pin_torque_in, w_torque);
}

void Motor_TRX::writeSpeed(int _SPEED) {
  if (DYNAMIC_TORQUE) {
    Motor_TRX::dynamicTorque(_SPEED, w_direction);
  }
  if (_SPEED < _speed_thereshold) {
    w_speed = 0;
    analogWrite(_pin_speed_in, w_speed);
    digitalWrite(_pin_speed_in_0, HIGH);
  } else {
    w_speed = _SPEED;
    digitalWrite(_pin_speed_in_0, LOW);
    analogWrite(_pin_speed_in, w_speed);
  }
}

void Motor_TRX::brake(int _time) {
  long start_time = millis();
  while (millis() < start_time + _time) {
    writeTorque(255);
    writeSpeed(255);
    writeDirection(!readDirection());
    deay(30);
  }
  writeTorque(0);
}

void Motor_TRX::dynamicTorque(int _SPEED, bool _DIRECTION) {

  if (_SPEED != w_speed || _DIRECTION != w_direction) {
    _last_change = millis();

    if (w_torque != _torque_max) {
      w_torque = _torque_max;
      writeTorque(w_torque);
      delay(_delay_torque_1);
    }
  }

  if (millis() >= _last_change + _delay_torque_2 && w_torque != _torque_min) {
    w_torque = _torque_min;
    writeTorque(w_torque);
  }

}
