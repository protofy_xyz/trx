// Protofy.xyz 2020

#include "Class_motor_TRX.h"

#define DEBUG true

#define RAMP 1
#define DIRECTION_CHANGE  2
#define ON_OFF            3
#define RANDOM            4
#define BRAKE             5
int MODE = BRAKE;

int time_change[5] = {0, 50, 1000, 1000, 2000};


#define PIN_ONOFF             3
#define PIN_DIRECTION         4
#define PIN_TORQUE_IN         5
#define PIN_TORQUE_IN_0       16
#define PIN_SPEED_IN          6
#define PIN_SPEED_IN_0        11
#define PIN_PULSE_OUT         20    //A7
#define PIN_TORQUE_OUT        62    //A8
#define PIN_DIRECTION_OUT     18    //A9

bool ENABLE = true;
bool DIRECTION = true;
int SPEED = 0;

bool increasing = true;
long prev_change;
int speed_adder = 1;

Motor_TRX motor = Motor_TRX();

void setup() {

  motor.configurePins(PIN_ONOFF, PIN_DIRECTION, PIN_TORQUE_IN, PIN_TORQUE_IN_0, PIN_SPEED_IN, PIN_SPEED_IN_0, PIN_PULSE_OUT, PIN_TORQUE_OUT, PIN_DIRECTION_OUT);

  attachInterrupt(digitalPinToInterrupt(PIN_PULSE_OUT), readPulse_ISR, FALLING);

  Serial.begin(115200);

  motor.setDynamicTorque(false);
  motor.setSpeedThereshold(10);
  motor.writeEnable(ENABLE);
  motor.writeDirection(DIRECTION);
  motor.writeTorque(0);

}

void loop() {

  if (Serial.available() > 0) {
    MODE = Serial.parseInt();
    Serial.println(MODE);
  }

  motor.debug(DEBUG);

  motor.readDirection();
  motor.readEncoder();
  motor.readTorque();

  motor.writeEnable(ENABLE);
  motor.writeDirection(DIRECTION);
  motor.writeSpeed(SPEED);

  if (millis() >= (prev_change + time_change[MODE])) {
    prev_change = millis();

    switch (MODE) {
      case RAMP:
        ENABLE = true;
        SPEED += speed_adder;

        if (SPEED >= 210) {
          increasing = false;
          speed_adder = -1;
          DIRECTION = !DIRECTION;
        }

        if (SPEED <= 0) {
          increasing = true;
          speed_adder = 1;
          DIRECTION = !DIRECTION;
        }
        break;

      case DIRECTION_CHANGE:
        //motor.brake(50);
        ENABLE = true;
        SPEED = 210;
        DIRECTION = !DIRECTION;
        break;

      case ON_OFF:
        ENABLE = true;
        if (SPEED == 0) {
          SPEED = 255;
        } else {
          SPEED = 0;
          DIRECTION = !DIRECTION;
          while (millis() < (prev_change + time_change[MODE])) {
            motor.brake(10);
          }
        }
        break;

      case RANDOM:
        ENABLE = true;
        motor.writeTorque(0);
        time_change[3] = random(5000, 8000);
        SPEED = 255;
        DIRECTION = random(0, 2);
        motor.writeSpeed(SPEED);
        motor.writeDirection(DIRECTION);
        delay(500);
        DIRECTION = !DIRECTION;
        motor.writeDirection(DIRECTION);
        delay(600);
        motor.brake(time_change[3] - 1000);
        SPEED = 0;
        break;

      case BRAKE:
        motor.brake(1000);
        break;

      default:
        ENABLE = false;
        MODE = 0;
        break;
    }
  }
}

void readPulse_ISR() {
  motor.newPulse = true;
  Serial.println("Pulse");
}
