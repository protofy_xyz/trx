#include "Arduino.h"

class Motor_TRX {

  public:

    Motor_TRX();
    void configurePins(int pin_on_off, int pin_direction_in, int pin_torque_in, int pin_torque_in_0, int pin_speed_in, int pin_speed_in_0, int pin_pulse_out, int pin_torque_out, int pin_direction_out);
    void setDynamicTorque(bool _DYNAMIC_TORQUE);
    void setSpeedThereshold(int speed_thereshold);
    void debug(bool TRX_DEBUG);

    volatile bool newPulse;
    int pulse_count;
    bool readEncoder();

    int  readSpeed();
    int  readTorque();
    bool readDirection();

    void writeEnable(bool ENABLE);
    void writeDirection(bool DIRECTION);
    void writeTorque(int _w_torque);
    void writeSpeed(int _w_speed);
    void brake(int _time);
    void dynamicTorque(int _SPEED, bool _DIRECTION);
    bool w_enable;
    int w_speed;
    int r_speed;
    int w_torque;
    int r_torque;
    bool w_direction;
    bool r_direction;

    void speedRamp(int speed_setpoint, int dt, int RAMP_UP);

  private:

    int _pin_on_off;
    int _pin_direction_in;
    int _pin_torque_in;
    int _pin_torque_in_0;
    int _pin_speed_in;
    int _pin_speed_in_0;
    int _pin_pulse_out;
    int _pin_torque_out;
    int _pin_direction_out;

    bool DYNAMIC_TORQUE = true;
    int _delay_torque_1 = 200;
    int _delay_torque_2 = 400;
    int _torque_min = 100;
    int _torque_max = 0;
    long _last_change;

    int _speed_thereshold;

    long _debug_print_period = 200;
    long _debug_print_previous;

    long _last_pulse;
    long _previous_pulse;

};
