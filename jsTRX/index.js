const MOTORTRX_DATA =        0x50 
const MOTORTRX_ATTACH =      0x00
const MOTORTRX_ENABLE =      0x01
const MOTORTRX_DIRECTION =   0x02
const MOTORTRX_SPEED =       0x03
const MOTORTRX_BRAKE =       0x04
const MOTORTRX_BRAKE1000 =   0x05



const firmata = require('firmata');

var arduinoPort;
var board;
var disconnectInterval;

createBoard = () => {
    firmata.Board.requestPort((error, port) => {
        if (error) {
            console.log(error);
            return;
        }

        arduinoPort = port;
        console.log('Using port: ', arduinoPort.comName);

        board = new firmata.Board(port.comName, function (err) {
        //board = new firmata.Board("/dev/cu.usbmodem142201", function (err) {
            if (err) {
                console.log('Error: ', err);
                return;
            }

            clearInterval(disconnectInterval);
        });

        board.transport.on("close", () => { console.log("close SP"); });

        board.on("connect", () => {
            console.log("connect");
        });

        board.on("disconnect", () => {
            console.log("disconnect");
            disconnectInterval = setInterval(() => {
                createBoard();
            }, 5000);
        });

        board.on("ready", () => {
            console.log("ready");
            setUpBoard(board);
        });
    });
}


timeout = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

setUpBoard = async (board) => {
    // board.sysexResponse(0x51, (data) => {
    //     console.log("Callback OK!");
    //     console.log("Data Received ", data);
    // });

    console.log("Sending data...");
    console.log("Attach");
    board.sysexCommand([MOTORTRX_DATA, MOTORTRX_ATTACH]);

    while (true){

        console.log("Direction: 0");
        board.sysexCommand([MOTORTRX_DATA, MOTORTRX_DIRECTION, 0]);
        console.log("Speed: 100");
        board.sysexCommand([MOTORTRX_DATA, MOTORTRX_SPEED, 100]);
        await timeout(2000);

        console.log("Enable");
        board.sysexCommand([MOTORTRX_DATA, MOTORTRX_ENABLE, 1]);
        await timeout(2000);
        
        console.log("Speed: 255");
        board.sysexCommand([MOTORTRX_DATA, MOTORTRX_SPEED, 255]);
        await timeout(2000);

        console.log("Direction: 1");
        board.sysexCommand([MOTORTRX_DATA, MOTORTRX_DIRECTION, 1]);
        await timeout(2000);

        console.log("Speed: 100");
        board.sysexCommand([MOTORTRX_DATA, MOTORTRX_SPEED, 100]);
        await timeout(2000);

        console.log("Speed: 255");
        board.sysexCommand([MOTORTRX_DATA, MOTORTRX_SPEED, 255]);
        await timeout(2000);

        // console.log("Brake");
        // board.sysexCommand([MOTORTRX_DATA, MOTORTRX_BRAKE, 2000]);
        // await timeout(2000);

        console.log("Brake");
        board.sysexCommand([MOTORTRX_DATA, MOTORTRX_BRAKE1000, 20]);
        await timeout(20000);

        console.log("Disable");
        board.sysexCommand([MOTORTRX_ENABLE, MOTORTRX_ENABLE, 0]);
        await timeout(2000);
    }
}

createBoard();