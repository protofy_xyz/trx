#include <ConfigurableFirmata.h>
#include "MotorTrx.h"
#include <string.h>
#include "FirmataMotorTrx.h"

#define isAttached() (motorTrx)

#define PIN_ONOFF             3
#define PIN_DIRECTION         4
#define PIN_TORQUE_IN         5
#define PIN_TORQUE_IN_0       16
#define PIN_SPEED_IN          6
#define PIN_SPEED_IN_0        11
#define PIN_PULSE_OUT         20    //A7
#define PIN_TORQUE_OUT        62    //A8
#define PIN_DIRECTION_OUT     18    //A9

static MotorTrx *motorTrx;

/* Constructor */
FirmataMotorTrx::FirmataMotorTrx()
{
  memset(motorTrx, 0, sizeof(MotorTrx *));
}

void FirmataMotorTrx::attachMotorTrx()
{
  if (isAttached())
  {
    return;
  }
  motorTrx = new MotorTrx();
}

void FirmataMotorTrx::detachMotorTrx()
{
  if (isAttached())
  {
    free(motorTrx);
    motorTrx = NULL;
  }
}

boolean FirmataMotorTrx::handlePinMode(byte pin, int mode)
{
  if (mode == OUTPUT)
  {
    if (IS_PIN_INTERRUPT(pin))
    {
      // nothing to do, pins states are managed
      // in "attach/detach Encoder" methods
      return true;
    }
  }
  return false;
}

void FirmataMotorTrx::handleCapability(byte pin) {
  if (IS_PIN_INTERRUPT(pin))
  {
    Firmata.write(OUTPUT);
    Firmata.write(8); //28 bits used for absolute position TO REVIEW
  }
}

boolean FirmataMotorTrx::handleSysex(byte command, byte argc, byte *argv) {
  byte subcommand = argv[0];

  switch (command) {
    case MOTORTRX_DATA:

      switch (subcommand) {
          int parameter;

        case MOTORTRX_ATTACH:
          attachMotorTrx();
          configurePins(PIN_ONOFF, PIN_DIRECTION, PIN_TORQUE_IN, PIN_TORQUE_IN_0, PIN_SPEED_IN, PIN_SPEED_IN_0, PIN_PULSE_OUT, PIN_TORQUE_OUT, PIN_DIRECTION_OUT);
          return true;
          break;

        case MOTORTRX_ENABLE:
          parameter = argv[1];
          writeEnable(parameter == 1);
          return true;
          break;

        case MOTORTRX_DIRECTION:
          parameter = argv[1];
          writeDirection(parameter == 1);
          return true;
          break;

        case MOTORTRX_SPEED:
          parameter = argv[1];
          writeSpeed(parameter);
          return true;
          break;

        case MOTORTRX_BRAKE:
          parameter = argv[1];
          brake(parameter);
          return true;
          break;

        case MOTORTRX_BRAKE1000:
          parameter = argv[1];
          brake1000(parameter);
          return true;
          break;

        default:
          return false;
          break;
      }

    default:
      return false;
      break;

  }
}

void FirmataMotorTrx::reset()
{
  detachMotorTrx();
  motorTrx->configurePins(PIN_ONOFF, PIN_DIRECTION, PIN_TORQUE_IN, PIN_TORQUE_IN_0, PIN_SPEED_IN, PIN_SPEED_IN_0, PIN_PULSE_OUT, PIN_TORQUE_OUT, PIN_DIRECTION_OUT);
}

boolean FirmataMotorTrx::isAttachedMotorTrx()
{
  return isAttached();
}

void FirmataMotorTrx::configurePins(int pin_on_off, int pin_direction_in, int pin_torque_in, int pin_torque_in_0, int pin_speed_in, int pin_speed_in_0, int pin_pulse_out, int pin_torque_out, int pin_direction_out) {
  motorTrx->configurePins(pin_on_off, pin_direction_in, pin_torque_in, pin_torque_in_0, pin_speed_in, pin_speed_in_0, pin_pulse_out, pin_torque_out, pin_direction_out);
}

void FirmataMotorTrx::writeEnable(bool w_enable) {
  motorTrx->writeEnable(w_enable);
}

void FirmataMotorTrx::writeDirection(bool w_direction) {
  motorTrx->writeDirection(w_direction);
}

void FirmataMotorTrx::writeSpeed(int w_speed) {
  motorTrx->writeSpeed(w_speed);
}

void FirmataMotorTrx::brake(int brake_time) {
  motorTrx->brake(brake_time);
}

void FirmataMotorTrx::brake1000(byte brake_time) {
  motorTrx->brake1000(brake_time);
}
