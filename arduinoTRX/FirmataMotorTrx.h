#ifndef FirmataMotorTrx_h
#define FirmataMotorTrx_h

#include <ConfigurableFirmata.h>
#include <FirmataFeature.h>

#include "MotorTrx.h"

#define MOTORTRX_DATA         0x50
#define MOTORTRX_ATTACH       0x00
#define MOTORTRX_ENABLE       0x01
#define MOTORTRX_DIRECTION    0x02
#define MOTORTRX_SPEED        0x03
#define MOTORTRX_BRAKE        0x04
#define MOTORTRX_BRAKE1000    0x05


class FirmataMotorTrx : public FirmataFeature
{
  public:
    FirmataMotorTrx();
    //~FirmataMotorTrx(); => never destroy in practice

    // FirmataFeature implementation
    boolean handlePinMode(byte pin, int mode);
    void handleCapability(byte pin);
    boolean handleSysex(byte command, byte argc, byte *argv);
    void reset();

    // FirmataMotorTrx implementation
    boolean isAttachedMotorTrx();
    void attachMotorTrx();
    void detachMotorTrx();

    bool readTorque();
    bool readDirection();

    void configurePins(int pin_on_off, int pin_direction_in, int pin_torque_in, int pin_torque_in_0, int pin_speed_in, int pin_speed_in_0, int pin_pulse_out, int pin_torque_out, int pin_direction_out);
    void writeEnable(bool w_enable);
    void writeDirection(bool w_direction);
    void writeSpeed(int w_speed);
    void brake(int brake_time);
    void brake1000(byte brake_time);

};

#endif
