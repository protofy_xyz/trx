#ifndef MotorTrx_h
#define MotorTrx_h

#include "Arduino.h"

class MotorTrx {
    
    public:

    MotorTrx();
    void configurePins(int pin_on_off, int pin_direction_in, int pin_torque_in, int pin_torque_in_0, int pin_speed_in, int pin_speed_in_0, int pin_pulse_out, int pin_torque_out, int pin_direction_out);
    
    bool readTorque();
    bool readDirection();

    void writeEnable(bool w_enable);
    void writeDirection(bool w_direction);
    void writeTorque(byte w_torque);
    void writeSpeed(byte w_speed);
    void brake(int brake_time);
    void brake1000(byte brake_time);


    private:

    int _pin_on_off;
    int _pin_direction_in;
    int _pin_torque_in;
    int _pin_torque_in_0;
    int _pin_speed_in;
    int _pin_speed_in_0;
    int _pin_pulse_out;
    int _pin_torque_out;
    int _pin_direction_out;

    bool _r_torque;
    bool _r_direction; 

    bool _w_enable;
    bool _w_direction;
    int _w_torque;
    int _w_speed;

    int _brake_time = 200;
};
#endif
