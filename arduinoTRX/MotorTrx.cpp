#include "Arduino.h"
#include "MotorTrx.h"


MotorTrx::MotorTrx() {
}

void MotorTrx::configurePins(int pin_on_off, int pin_direction_in, int pin_torque_in, int pin_torque_in_0, int pin_speed_in, int pin_speed_in_0, int pin_pulse_out, int pin_torque_out, int pin_direction_out) {

  _pin_on_off         = pin_on_off;
  _pin_direction_in   = pin_direction_in;
  _pin_torque_in      = pin_torque_in;
  _pin_torque_in_0    = pin_torque_in_0;
  _pin_speed_in       = pin_speed_in;
  _pin_speed_in_0     = pin_speed_in_0;
  _pin_pulse_out      = pin_pulse_out;
  _pin_torque_out     = pin_torque_out;
  _pin_direction_out  = pin_direction_out;

  pinMode(_pin_on_off, OUTPUT);
  pinMode(_pin_direction_in, OUTPUT);
  pinMode(_pin_torque_in, OUTPUT);
  pinMode(_pin_torque_in_0, OUTPUT);
  pinMode(_pin_speed_in, OUTPUT);
  pinMode(_pin_speed_in_0, OUTPUT);
  pinMode(_pin_pulse_out, INPUT);
  pinMode(_pin_torque_out, INPUT);
  pinMode(_pin_direction_out, INPUT);

  writeEnable(false);
  writeSpeed(0);
  writeTorque(0);
}

bool MotorTrx::readTorque() {
  _r_torque = digitalRead(_pin_torque_out);
  return _r_torque;
}

bool MotorTrx::readDirection() {
  _r_direction = digitalRead(_pin_direction_out);
  return _r_direction;
}

void MotorTrx::writeEnable(bool w_enable) {
  _w_enable = w_enable;
  digitalWrite(_pin_on_off, _w_enable);
}

void MotorTrx::writeDirection(bool w_direction) {
  _w_direction = w_direction;
  digitalWrite(_pin_direction_in, _w_direction);
}

void MotorTrx::writeTorque(byte w_torque) {
  _w_torque = w_torque;
  analogWrite(_pin_torque_in, _w_torque);
}

void MotorTrx::writeSpeed(byte w_speed) {
  _w_speed = w_speed;
  analogWrite(_pin_speed_in, _w_speed);
}

void MotorTrx::brake(int brake_time) {
  long start_time = millis();
  writeTorque(255);
  writeSpeed(255);
  while (millis() < (start_time + brake_time)) {
    writeDirection(!readDirection());
  }
  writeTorque(0);
  writeSpeed(0);
}

void MotorTrx::brake1000(byte brake_time) {
  brake(1000 * brake_time);
}
